USE [SAVDB]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[sp_sav_test_Mapping]
(
--DECLARE
	@Code		VARCHAR(101) 	= 'NGuyen tr',
	@FullName	NVARCHAR(100)	,
	@Phone		VARCHAR(20)		,
	@PhoneChar		CHAR(60)		,
	@PhoneNChar		NCHAR(60)		,
	@Age		INT		,
	@IsDefault	BIT		,
	@Amount		FLOAT		,
	@Day		DATETIME		
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	SELECT *
	FROM [User]
END