USE [SAVDB]
 GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_wallettype_SetDefault]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_wallettype_SetDefault]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallettype_SetDefault]
(
--DECLARE
	@Id			INT,
	@UserCode	VARCHAR(400)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Id,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Id] = @Id AND [IsDeleted] = 0 AND [IsDefault] = 0 AND [UserCode] = @UserCode) = 0 
			BEGIN
				SET @ErrorMessage  = 'The key does not exist or has been deleted.'

				IF ISNULL(@Id,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The key of wallet type must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage  = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The user code must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE T
			SET T.[IsDefault] = 0,
				T.[LastModified] = GETDATE()
			FROM [dbo].[WalletType] T
			WHERE
				[UserCode] = @UserCode
				AND [IsDeleted] = 0
				AND [IsDefault] = 1

			UPDATE T
			SET T.[IsDefault] = 1,
				T.[LastModified] = GETDATE()
			FROM [dbo].[WalletType] T
			WHERE
				[UserCode] = @UserCode
				AND [IsDefault] = 0
				AND [IsDeleted] = 0
				AND [Id] = @Id
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END