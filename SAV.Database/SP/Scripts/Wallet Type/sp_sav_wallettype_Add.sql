USE [SAVDB]
 GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_wallettype_Add]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_wallettype_Add]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallettype_Add]
(
--DECLARE
	@Key		NVARCHAR(70),
	@Name		NVARCHAR(500),
	@UserCode	VARCHAR(400)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Key,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Key] = UPPER(@Key) AND [IsDeleted] = 0 AND [UserCode] = @UserCode) > 0 
			BEGIN
				SET @ErrorMessage  = 'The code is (' + @Key + ') cannot be duplicated.'

				IF ISNULL(@Key,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The key of wallet type must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Name,'') = ''
			BEGIN
				SET @ErrorMessage  = 'The name of wallet type must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage  = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The user code must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			INSERT INTO [dbo].[WalletType]
			(
				[Key]		
				,[Name]		
				,[IsDeleted] 
				-- ,[IsDefault] 
				,[UserCode]
			)
			VALUES
			(
				UPPER(@Key)		
				,@Name		
				-- ,0 
				,0 
				,@UserCode
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 
			[Id],
			[Key],
			[Name],
			-- [IsDefault],
			[IsDeleted],
			[UserCode]
		FROM [dbo].[WalletType]
		WHERE [Key] = @Key
			AND [Name] = @Name
			AND [IsDeleted] = 0
			AND [UserCode] = @UserCode

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END