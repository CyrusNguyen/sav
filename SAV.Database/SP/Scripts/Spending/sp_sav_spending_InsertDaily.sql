GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_spending_InsertDaily]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_spending_InsertDaily]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.06.16 11:25 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_spending_InsertDaily]
(
--DECLARE
	@Code				VARCHAR(70)				 ,
	@Title				NVARCHAR(100)		 	 ,
	@Description		NVARCHAR(500)	= NULL 	 ,
	@Expense			FLOAT					 ,
	@PaymentDate		DATETIME				 ,
	@WalletCode			VARCHAR(70)				 ,
	@UserCode			VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF	  
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Code,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Spending] WHERE [Code] = @Code AND [IsDeleted] = 0 AND [UserCode] = @UserCode) <> 0
			BEGIN
				SET @ErrorMessage = 'The code is (' + @Code + ') cannot be duplicated.'

				IF ISNULL(@Code,'') = ''
				BEGIN
					SET @ErrorMessage = 'The code of spending must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Title,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The title of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@PaymentDate,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The payment date of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @Expense <= 0 OR ISNULL(@Expense,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The expense of spending must not be empty.'

				IF @Expense <= 0
				BEGIN
					SET @ErrorMessage = 'The expense no less than 0.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END
			
			IF (SELECT [Balance] FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) < @Expense
			BEGIN
				SET @ErrorMessage = 'The expense no less than your balance.'

				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			DECLARE 
				@BalanceBase FLOAT

			INSERT INTO [dbo].[Spending]
			(
				[Code],
				[Title],
				[Description],
				[Expense],
				[PaymentDate],
				[Month],
				[Year],
				[IsDeleted],
				[UserCode]
			)
			VALUES
			(
				@Code,
				@Title,
				@Description,
				@Expense,
				@PaymentDate,
				MONTH(@PaymentDate),
				YEAR(@PaymentDate),
				0,
				@UserCode
			)

			SELECT @BalanceBase = [Balance]
			FROM [dbo].[Wallet]
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [USerCode] = @UserCode

			INSERT INTO [dbo].[SpendingHistory]
			(
				[WalletCode]		
				,[SpendingCode]		
				,[Expense]				
				,[ActionCode]		
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode,
				@Code,
				@Expense,
				'CREATED',
				1,
				@UserCode
			)

			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode
				,@Code
				,@BalanceBase		
				,@Expense		
				,@BalanceBase - @Expense	
				,@Title
				,'EXIT'		
				,1	
				,1
				,@UserCode
			)

			UPDATE W
			SET [Balance] = [Balance] - @Expense
				,[LastModified] = GETDATE()
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
		END 
		-- END BODY

		COMMIT TRAN 

		SELECT 
			[Id],
			[Code],
			[Title],
			[Expense],
			[Description],
			[PaymentDate],
			[UserCode]
		FROM [dbo].[Spending]
		WHERE [Code] = @Code
			AND [UserCode] = @UserCode

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END