GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_spending_UpdateDaily]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_spending_UpdateDaily]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.16 09:41 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_spending_UpdateDaily]
(
--DECLARE
	@Code				NVARCHAR(70)			 ,
	@Title				NVARCHAR(100)		 	 ,
	@Description		NVARCHAR(500)	= NULL 	 ,
	@Expense			FLOAT					 ,
	@PaymentDate		DATETIME				 ,
	@WalletCode			NVARCHAR(70)			 ,
	@UserCode			VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF	  
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		DECLARE 
				@ExpenseBase FLOAT,
				@BalanceBase FLOAT,
				@BalanceAdd FLOAT,
				@WalletCodeBase	 NVARCHAR(70),
				@BalanceNext FLOAT

		SELECT 
			@ExpenseBase = [Expense],
			@WalletCodeBase	=  H.[WalletCode]
		FROM [dbo].[SpendingHistory] H
		WHERE [SpendingCode] = @Code
			AND [IsActived] = 1
			AND [UserCode] = @UserCode
		
		SELECT 
			@BalanceBase = [Balance]
		FROM [dbo].[Wallet] W
		WHERE [Code] = @WalletCodeBase
			AND [IsDeleted] = 0
			AND [UserCode] = @UserCode

		SELECT 
			@BalanceNext = [Balance]
		FROM [dbo].[Wallet] W
		WHERE [Code] = @WalletCode
			AND [IsDeleted] = 0
			AND [UserCode] = @UserCode

		BEGIN -- BEGIN VERIFY PARAMETER
			IF @WalletCode = @WalletCodeBase
			BEGIN
				IF (@ExpenseBase + @BalanceBase) < @Expense
				BEGIN
					SET @ErrorMessage = 'The balance code "' + @WalletCode + '" just have "' + CAST((@ExpenseBase + @BalanceBase) AS NVARCHAR) + '". Not enough to payment for this item.'

					RAISERROR(@ErrorMessage,16,1)
				END 
			END
			ELSE 
			BEGIN
				IF @BalanceNext < @Expense
				BEGIN
					SET @ErrorMessage = 'The balance code "' + @WalletCode + '" just have "' + CAST((@BalanceNext) AS NVARCHAR) + '". Not enough to payment for this item.'

					RAISERROR(@ErrorMessage,16,1)
				END 
			END

			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Code,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Spending] WHERE [Code] = @Code AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The code does not exist or has been deleted.'

				IF ISNULL(@Code,'') = ''
				BEGIN
					SET @ErrorMessage = 'The code of spending must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Title,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The title of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@PaymentDate,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The payment date of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Expense,'') = '' OR @Expense <= 0
			BEGIN
				SET @ErrorMessage = 'The expense of spending must not be empty.'

				IF @Expense <= 0
				BEGIN
					SET @ErrorMessage = 'The expense no less than 0.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END  

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
		
			If OBJECT_ID('tempdb..#TmpWalletHistory') IS NOT NULL
				DROP TABLE #TmpWalletHistory
			SELECT 
				[WalletCode]	
				,[ReferenceCode]
				,@BalanceBase AS [Amount]		
				,[Expense]		
				,@BalanceBase + @ExpenseBase AS [Balance]		
				,[Description]	
				,GETDATE() [CreatedDate]
				,GETDATE() AS [LastModified]
				,'ENTRY' AS [WayCode]		
				,[IsSpending]
				,0 AS [IsActived]
				,[UserCode]
			INTO #TmpWalletHistory
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [UserCode] = @UserCode
				
			UPDATE H
			SET	[IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [UserCode] = @UserCode
		
			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[CreatedDate]
				,[LastModified]
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			SELECT 
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[CreatedDate]
				,[LastModified]
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode] 
			FROM #TmpWalletHistory		

			UPDATE H
			SET [IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[SpendingHistory] H
			WHERE [SpendingCode] = @Code
				AND [IsActived] = 1
				AND [UserCode] = @UserCode

			INSERT INTO [dbo].[SpendingHistory]
			(
				[WalletCode]		
				,[SpendingCode]		
				,[Expense]				
				,[ActionCode]		
				,[IsActived]	
				,[UserCode]
			)
			VALUES
			(
				@WalletCode,
				@Code,
				@Expense,
				'UPDATED',
				1,
				@UserCode
			)

			UPDATE W
			SET [Balance] = @BalanceBase + @ExpenseBase,
				[LastModified] = GETDATE()
			FROM [dbo].Wallet W
			WHERE [Code] = @WalletCodeBase
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
		
			SELECT 
				@BalanceAdd = [Balance]
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [UserCode] = @UserCode

			INSERT INTO [dbo].[WalletHistory]
			(
				 [WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode	
				,@Code
				,@BalanceAdd		
				,@Expense		
				,@BalanceAdd - @Expense		
				,@Title	
				,'EXIT'		
				,1
				,1
				,@UserCode
			)

			UPDATE W
			SET [Balance] = [Balance] - @Expense,
				[LastModified] = GETDATE()
			FROM [dbo].Wallet W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode

			UPDATE W
			SET [Expense] = @Expense,
				[Title] = @Title,
				[Description] = @Description,
				[PaymentDate] = @PaymentDate,
				[Month] = MONTH(@PaymentDate),
				[Year] = YEAR(@PaymentDate),
				[LastModified] = GETDATE()
			FROM [dbo].[Spending] W
			WHERE [Code] = @Code
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
			
			DROP TABLE #TmpWalletHistory
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END