GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_spending_DeleteDaily]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_spending_DeleteDaily]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.06.16 11:25 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
--=============================================
CREATE PROC [dbo].[sp_sav_spending_DeleteDaily]
(	
--DECLARE
	@Code			VARCHAR(70),
	@UserCode		VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY 
		BEGIN TRAN -- BEGIN TRANSACTION

		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of user must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Code,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Spending] WHERE [Code] = @Code AND [IsDeleted] =  0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The code of spending does not exist or has been deleted.'

				IF ISNULL(@Code,'') = ''
				BEGIN
					SET @ErrorMessage = 'The code of spending must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER 

		BEGIN -- BEGIN BODY
			DECLARE 
				@WalletCodeBase NVARCHAR(70),
				@ExpenseBase FLOAT,
				@BalanceBase FLOAT

			UPDATE S
			SET [IsDeleted]	   = 1,
				[LastModified] = GETDATE()
			FROM [dbo].[Spending] S
			WHERE [Code] = @Code
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
		
			UPDATE H
			SET @WalletCodeBase = [WalletCode],
				@ExpenseBase = [Expense],
				[IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[SpendingHistory] H
			WHERE [IsActived] = 1
				AND SpendingCode = @Code
				AND [UserCode] = @UserCode
		
			INSERT INTO [dbo].[SpendingHistory]
			(
				[WalletCode]		
				,[SpendingCode]		
				,[Expense]
				,[ActionCode]		
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCodeBase,
				@Code,
				@ExpenseBase,
				'DELETED',
				1,
				@UserCode
			)

			UPDATE W
			SET [Balance] = [Balance] + @ExpenseBase,
				@BalanceBase = [Balance],
				[LastModified] = GETDATE()
			FROM [dbo].Wallet W
			WHERE [Code] = @WalletCodeBase
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
					
			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[CreatedDate]
				,[LastModified]
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			SELECT 
				[WalletCode]	
				,[ReferenceCode]
				,@BalanceBase AS [Amount]		
				,[Expense]		
				,@BalanceBase + @ExpenseBase AS [Balance]		
				,[Description]	
				,GETDATE() AS [CreatedDate]
				,GETDATE() AS [LastModified]
				,'ENTRY' AS [WayCode]		
				,[IsSpending]
				,1 AS [IsActived]
				,[UserCode]
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [UserCode] = @UserCode
		
			UPDATE H
			SET	[IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [WayCode] <> 'ENTRY'
				AND [UserCode] = @UserCode
		END
		-- END BODY

		COMMIT TRAN

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH	
END