USE [SAVDB]
GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_user_SignUp]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_user_SignUp]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.20 14:13 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Created
 --=============================================
CREATE PROC [dbo].[sp_sav_user_SignUp]
(
--DECLARE
	@Code		VARCHAR(100) 	,
	@Username	VARCHAR(500) 	,
	@Password	VARCHAR(500) 	,
	@TokenCode	VARCHAR(500) 	,
	@FullName	NVARCHAR(100)	,
	@Phone		VARCHAR(20)		,
	@Role		VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Code,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The code of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Username,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The username of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Password,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The password of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@TokenCode,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The token of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@FullName,'') = ''  
			BEGIN
				SET @ErrorMessage  = 'The full name of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Phone,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The phone of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Phone,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The phone of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Username] = @Username) > 0 
			BEGIN
				SET @ErrorMessage  = 'The username is (' + @Username + ') cannot be duplicated.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @Code) > 0 
			BEGIN
				SET @ErrorMessage  = 'The code is (' + @Code + ') cannot be duplicated.'
				RAISERROR(@ErrorMessage,16,1)
			END
			
			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [TokenCode] = @TokenCode) > 0 
			BEGIN
				SET @ErrorMessage  = 'The token code is (' + @TokenCode + ') cannot be duplicated.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			INSERT INTO [dbo].[User]
			(
				[Code]		
				,[Username]	
				,[Password]	
				,[TokenCode] 
				,[FullName]  
				,[Phone]
				,[Role]
			)
			VALUES
			(
				@Code		
				,@Username
				,CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)
				,@TokenCode
				,@FullName  
				,@Phone		
				,@Role
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT	
			[Code]
			,[Username]
			,[FullName]
			,[TokenCode]
			,[Phone]
			,[Role]
		FROM [dbo].[User] R
		WHERE 
			[Code] = @Code	
			AND [Username] = @Username
			AND [TokenCode] = @TokenCode
			AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,@Code)
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END