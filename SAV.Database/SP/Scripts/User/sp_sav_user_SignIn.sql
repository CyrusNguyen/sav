USE [SAVDB]
GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_user_SignIn]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_user_SignIn]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.20 14:13 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Created
 --=============================================
CREATE PROC [dbo].[sp_sav_user_SignIn]
(
--DECLARE
	@Username	VARCHAR(500) 	    ,
	@Password	VARCHAR(500) 	    
)AS
BEGIN
	
	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Username] = @Username AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)) = 0 
			BEGIN
				SET @ErrorMessage  = 'The username is (' + @Username + ') cannot exist or password wrong.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER
		
		SELECT	
			[Code]
			,[Username]
			,[FullName]
			,[TokenCode]
			,[Phone]
			,[Role]
		FROM [dbo].[User] R
		WHERE 
			[Username] = @Username
			AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@Username,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH

END