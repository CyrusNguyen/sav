USE [SAVDB]
 GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_wallet_AddMoney]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_wallet_AddMoney]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.18 14:57 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_AddMoney]
(
--DECLARE
	@WalletCode		VARCHAR(70)			 ,
	@Amount			FLOAT				 ,
	@Description	NVARCHAR(500)	= NULL,
	@UserCode		VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of spending must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @Amount <= 0 OR ISNULL(@Amount,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The amount of wallet must not be empty.'

				IF @Amount <= 0
				BEGIN
					SET @ErrorMessage = 'The amount no less than 0.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			DECLARE
				@BalanceBase FLOAT

			UPDATE W
			SET W.[Balance] = [Balance] + @Amount,
				@BalanceBase = [Balance],
				[LastModified] = GETDATE()
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
			
			INSERT INTO [dbo].[WalletHistory]
			(
				 [WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode	
				,@WalletCode
				,@BalanceBase		
				,@Amount		
				,@BalanceBase + @Amount		
				,@Description
				,'ENTRY'		
				,0
				,1
				,@UserCode
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END