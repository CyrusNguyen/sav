USE [SAVDB]
 GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_wallet_MoveMoney]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_wallet_MoveMoney]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.18 11:05 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_MoveMoney]
(
--DECLARE
	@WalletCode			VARCHAR(70)		 ,
	@ReferenceCode		VARCHAR(500)			 ,
	@Description		NVARCHAR(500)	= NULL 	 ,
	@Expense			FLOAT					 ,
	@UserCode			VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF	  
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of spending must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @Expense <= 0 OR ISNULL(@Expense,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The expense of wallet must not be empty.'

				IF @Expense <= 0
				BEGIN
					SET @ErrorMessage = 'The expense no less than 0.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@ReferenceCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @ReferenceCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The reference code does not exist or has been deleted.'

				IF ISNULL(@ReferenceCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The reference code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF (SELECT [Balance] FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) < @Expense
			BEGIN
				SET @ErrorMessage = 'The expense no greate than your balance.'

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @WalletCode = @ReferenceCode
			BEGIN
				SET @ErrorMessage = 'The source wallet and destination wallet must be different.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			DECLARE 
				@BalanceWalletBase FLOAT,
				@BalanceReferenceBase FLOAT,
				@NameWalletBase NVARCHAR(200),
				@NameReferenceBase NVARCHAR(200)
		
			UPDATE W
			SET @BalanceWalletBase = [Balance],
				@NameWalletBase = [Name],
				[Balance] = [Balance] - @Expense
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode

			UPDATE W
			SET @BalanceReferenceBase = [Balance],
				@NameReferenceBase = [Name],
				[Balance] = [Balance] + @Expense
			FROM [dbo].[Wallet] W
			WHERE [Code] = @ReferenceCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode

			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			SELECT 
				@WalletCode [WalletCode],
				@ReferenceCode [ReferenceCode],
				@BalanceWalletBase [Amount],
				@Expense [Expense],
				@BalanceWalletBase - @Expense [Balance],
				CASE 
					WHEN @Description IS NULL OR @Description = '' THEN 'Move money from wallet "' + @NameWalletBase + '" to "' + @NameReferenceBase + '"'
					ELSE @Description END [Description],
				'EXIT' [WayCode],
				0 [IsSpending],
				1 [IsActived],
				@UserCode
			UNION 
			SELECT 
				@ReferenceCode [WalletCode],
				@WalletCode [ReferenceCode],
				@BalanceReferenceBase [Amount],
				@Expense [Expense],
				@BalanceReferenceBase + @Expense [Balance],
				CASE 
					WHEN @Description IS NULL OR @Description = '' THEN 'Move money from wallet "' + @NameReferenceBase + '" to "' + @NameWalletBase + '"'
					ELSE @Description END [Description],
				'ENTRY' [WayCode],
				0 [IsSpending],
				1 [IsActived],
				@UserCode
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END