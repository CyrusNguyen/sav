USE [SAVDB]
 GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_wallet_Update]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_wallet_Update]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_Update]
(
--DECLARE
	@WalletCode		VARCHAR(70)			 ,
	@Name			NVARCHAR(50)		 ,
	@UserCode		VARCHAR(100)		 ,
	@TypeCode		INT
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@TypeCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Id] = @TypeCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The type wallet code does not exist or has been deleted.'

				IF ISNULL(@TypeCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The type wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Name,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The name wallet must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE [dbo].[Wallet] 
			SET	[Name]	= @Name	
				,[TypeCode] = @TypeCode
				,[LastModified] = GETDATE()
			FROM [dbo].[Wallet] 
			WHERE 
				[Code] = @WalletCode	
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode	
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END