 SELECT  
	REPLACE(p.[name], '@', '') AS [Name] ,  
	t.[name] AS [Type], 
	CAST(p.[max_length] as int) AS [Length],*
FROM [sys].[parameters] p  
	LEFT JOIN [sys].[types] t on p.system_type_id = t.system_type_id AND p.user_type_id = t.user_type_id 

WHERE p.[object_id] = OBJECT_ID('sp_sav_test_Mapping')