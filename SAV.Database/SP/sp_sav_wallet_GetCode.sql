GO
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID (N'[sp_sav_wallet_GetCode]') AND type = N'P')
	DROP PROCEDURE [dbo].[sp_sav_wallet_GetCode]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.16 09:41 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_GetCode]
(
--DECLARE
	@Code NVARCHAR(600)
)AS
BEGIN
	
	SELECT [Name],*
	from [Wallet]
	Where Code = @Code
END

--12	B	a	12	2019-01-01 00:00:00.000	2019-01-01 00:00:00.000	True	False	A	a