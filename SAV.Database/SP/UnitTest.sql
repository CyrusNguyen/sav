--EXEC [dbo].[sp_sav_wallet_MoveMoney]
--	@WalletCode	 = 'W111-111',
--	@ReferenceCode = 'W111-112',
--	@Expense		 = 398000,
--	@UserCode	 = 'U111-111'

--EXEC [dbo].[sp_sav_wallet_SubMoney]
--	@WalletCode	 = 'W111-112',
--	@Amout		 = 1000,
--	@Description = 'An sang thoi ma',
--	@UserCode	 = 'U111-111'

--EXEC [dbo].[sp_sav_wallet_AddMoney]
--	@WalletCode	 = 'W111-111',
--	@Amout		 = 100000,
--	@Description = 'An sang thoi ma',
--	@UserCode	 = 'U111-111'

--EXEC [sp_sav_wallet_Delete]
--	@WalletCode	   = 'W111-111'
--	,@UserCode	   = 'U111-111'


--	@Code			= 'SP111-111-111' 
--	,@Title			= 'Com ga' 
--	,@Description	= '' 
--	,@Expense		= 10000 
--	,@PaymentDate	= '20190707'
--	,@WalletCode	= '' 
--	,@UserCode		= '' 

--EXEC [sp_sav_wallettype_Add]
--	@Key		 = 'MMR'	 ,
--	@Name		 = 'Management More Time' ,
--	@UserCode	 = 'U111-112' 

--EXEC [dbo].[sp_sav_wallettype_Delete]
--	@Key		= 'MMR'	
--	,@UserCode	= 'U111-112'

--EXEC [sp_sav_wallet_Add]
--	@WalletCode	   = 'W111-111'
--	,@Name		   = N'Ví điện tử'
--	,@UserCode	   = 'U111-112'
--	,@TypeCode	   = 'SAV'

--EXEC [sp_sav_wallet_Update]
--	@WalletCode	   = 'W111-111'
--	,@Name		   = 'Ví ôm'
--	,@UserCode	   = 'U111-112'
--	,@TypeCode	   = 'SAV'







--EXEC [dbo].[sp_sav_spending_InsertDaily]
--	@Code			 = 'U-112'
--	,@Title			 = 'A'
--	,@Description	 = 'B'
--	,@Expense		 = 5000
--	,@PaymentDate	 = '20190101'
--	,@WalletCode	 = 'W111-112'
--	,@UserCode		 = 'U111-111'

--EXEC [dbo].[sp_sav_spending_DeleteDaily]
--	@Code		  = 'U-115',
--	@UserCode	  = 'U111-111'


EXEC [dbo].[sp_sav_spending_UpdateDaily]
	@Code			 = 'U-1111'
	,@Title			 = 'C'
	,@Description	 = 'D'
	,@Expense		 = 30001
	,@PaymentDate	 = '20190707'
	,@WalletCode	 = 'W111-112'
	,@UserCode		 = 'U111-111'

--EXEC [dbo].[sp_sav_spending_InsertDaily]
--	@Code			 = 'U-1111'
--	,@Title			 = 'A'
--	,@Description	 = 'B'
--	,@Expense		 = 5000
--	,@PaymentDate	 = '20190101'
--	,@WalletCode	 = 'W111-112'
--	,@UserCode		 = 'U111-111'

--SELECT * -- DELET
--FROM [dbo].[User]

SELECT * -- DELETE
FROM [dbo].[Spending]

SELECT * -- DELETE
FROM [dbo].[SpendingHistory]

SELECT * -- DELETE
FROM [dbo].[Wallet]

SELECT * -- DELETE
FROM [dbo].[WalletHistory]

--SELECT * -- DELETE
--FROM [dbo].[WalletType]

--SELECT * -- DELETE
--FROM [dbo].[Logger]
