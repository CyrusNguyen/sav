USE [master]
GO
/****** Object:  Database [SAVDB]    Script Date: 11/25/2019 10:14:50 AM ******/
CREATE DATABASE [SAVDB]
GO
USE [SAVDB]
GO
CREATE TABLE [dbo].[Logger](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Method] [nvarchar](500) NULL,
	[Message] [nvarchar](4000) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[UserCode] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Spending]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Spending](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](max) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[Expense] [float] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[UserCode] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SpendingHistory]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpendingHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WalletCode] [varchar](max) NOT NULL,
	[SpendingCode] [varchar](max) NOT NULL,
	[Expense] [float] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[ActionCode] [varchar](20) NOT NULL,
	[IsActived] [bit] NOT NULL,
	[UserCode] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Code] [varchar](100) NOT NULL,
	[Username] [varchar](500) NOT NULL,
	[Password] [varchar](500) NOT NULL,
	[TokenCode] [varchar](500) NOT NULL,
	[Role] [varchar](100) NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[Phone] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Wallet]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](max) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Balance] [float] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[TypeCode] [int] NULL,
	[UserCode] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WalletHistory]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WalletHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WalletCode] [varchar](max) NOT NULL,
	[ReferenceCode] [varchar](max) NOT NULL,
	[Amount] [float] NOT NULL,
	[Expense] [float] NOT NULL,
	[Balance] [float] NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[WayCode] [varchar](20) NOT NULL,
	[IsSpending] [bit] NOT NULL,
	[IsActived] [bit] NOT NULL,
	[UserCode] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WalletType]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WalletType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [varchar](max) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModified] [datetime] NULL,
	[UserCode] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Logger] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Logger] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[Spending] ADD  DEFAULT (datepart(month,getdate())) FOR [Month]
GO
ALTER TABLE [dbo].[Spending] ADD  DEFAULT (datepart(year,getdate())) FOR [Year]
GO
ALTER TABLE [dbo].[Spending] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Spending] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[SpendingHistory] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SpendingHistory] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[Wallet] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Wallet] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[WalletHistory] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[WalletHistory] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[WalletType] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[WalletType] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_spending_DeleteDaily]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.06.16 11:25 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
--=============================================
CREATE PROC [dbo].[sp_sav_spending_DeleteDaily]
(	
--DECLARE
	@Code			VARCHAR(70),
	@UserCode		VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY 
		BEGIN TRAN -- BEGIN TRANSACTION

		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of user must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Code,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Spending] WHERE [Code] = @Code AND [IsDeleted] =  0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The code of spending does not exist or has been deleted.'

				IF ISNULL(@Code,'') = ''
				BEGIN
					SET @ErrorMessage = 'The code of spending must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER 

		BEGIN -- BEGIN BODY
			DECLARE 
				@WalletCodeBase NVARCHAR(70),
				@ExpenseBase FLOAT,
				@BalanceBase FLOAT

			UPDATE S
			SET [IsDeleted]	   = 1,
				[LastModified] = GETDATE()
			FROM [dbo].[Spending] S
			WHERE [Code] = @Code
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
		
			UPDATE H
			SET @WalletCodeBase = [WalletCode],
				@ExpenseBase = [Expense],
				[IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[SpendingHistory] H
			WHERE [IsActived] = 1
				AND SpendingCode = @Code
				AND [UserCode] = @UserCode
		
			INSERT INTO [dbo].[SpendingHistory]
			(
				[WalletCode]		
				,[SpendingCode]		
				,[Expense]
				,[ActionCode]		
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCodeBase,
				@Code,
				@ExpenseBase,
				'DELETED',
				1,
				@UserCode
			)

			UPDATE W
			SET [Balance] = [Balance] + @ExpenseBase,
				@BalanceBase = [Balance],
				[LastModified] = GETDATE()
			FROM [dbo].Wallet W
			WHERE [Code] = @WalletCodeBase
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
					
			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[CreatedDate]
				,[LastModified]
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			SELECT 
				[WalletCode]	
				,[ReferenceCode]
				,@BalanceBase AS [Amount]		
				,[Expense]		
				,@BalanceBase + @ExpenseBase AS [Balance]		
				,[Description]	
				,GETDATE() AS [CreatedDate]
				,GETDATE() AS [LastModified]
				,'ENTRY' AS [WayCode]		
				,[IsSpending]
				,1 AS [IsActived]
				,[UserCode]
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [UserCode] = @UserCode
		
			UPDATE H
			SET	[IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [WayCode] <> 'ENTRY'
				AND [UserCode] = @UserCode
		END
		-- END BODY

		COMMIT TRAN

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_spending_InsertDaily]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.06.16 11:25 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_spending_InsertDaily]
(
--DECLARE
	@Code				VARCHAR(70)				 ,
	@Title				NVARCHAR(100)		 	 ,
	@Description		NVARCHAR(500)	= NULL 	 ,
	@Expense			FLOAT					 ,
	@PaymentDate		DATETIME				 ,
	@WalletCode			VARCHAR(70)				 ,
	@UserCode			VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF	  
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Code,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Spending] WHERE [Code] = @Code AND [IsDeleted] = 0 AND [UserCode] = @UserCode) <> 0
			BEGIN
				SET @ErrorMessage = 'The code is (' + @Code + ') cannot be duplicated.'

				IF ISNULL(@Code,'') = ''
				BEGIN
					SET @ErrorMessage = 'The code of spending must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Title,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The title of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@PaymentDate,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The payment date of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @Expense <= 0 OR ISNULL(@Expense,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The expense of spending must not be empty.'

				IF @Expense <= 0
				BEGIN
					SET @ErrorMessage = 'The expense no less than 0.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END
			
			IF (SELECT [Balance] FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) < @Expense
			BEGIN
				SET @ErrorMessage = 'The expense no less than your balance.'

				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			DECLARE 
				@BalanceBase FLOAT

			INSERT INTO [dbo].[Spending]
			(
				[Code],
				[Title],
				[Description],
				[Expense],
				[PaymentDate],
				[Month],
				[Year],
				[IsDeleted],
				[UserCode]
			)
			VALUES
			(
				@Code,
				@Title,
				@Description,
				@Expense,
				@PaymentDate,
				MONTH(@PaymentDate),
				YEAR(@PaymentDate),
				0,
				@UserCode
			)

			SELECT @BalanceBase = [Balance]
			FROM [dbo].[Wallet]
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [USerCode] = @UserCode

			INSERT INTO [dbo].[SpendingHistory]
			(
				[WalletCode]		
				,[SpendingCode]		
				,[Expense]				
				,[ActionCode]		
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode,
				@Code,
				@Expense,
				'CREATED',
				1,
				@UserCode
			)

			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode
				,@Code
				,@BalanceBase		
				,@Expense		
				,@BalanceBase - @Expense	
				,@Title
				,'EXIT'		
				,1	
				,1
				,@UserCode
			)

			UPDATE W
			SET [Balance] = [Balance] - @Expense
				,[LastModified] = GETDATE()
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
		END 
		-- END BODY

		COMMIT TRAN 

		SELECT 
			[Id],
			[Code],
			[Title],
			[Expense],
			[Description],
			[PaymentDate],
			[UserCode]
		FROM [dbo].[Spending]
		WHERE [Code] = @Code
			AND [UserCode] = @UserCode

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_spending_UpdateDaily]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.16 09:41 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_spending_UpdateDaily]
(
--DECLARE
	@Code				NVARCHAR(70)			 ,
	@Title				NVARCHAR(100)		 	 ,
	@Description		NVARCHAR(500)	= NULL 	 ,
	@Expense			FLOAT					 ,
	@PaymentDate		DATETIME				 ,
	@WalletCode			NVARCHAR(70)			 ,
	@UserCode			VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF	  
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		DECLARE 
				@ExpenseBase FLOAT,
				@BalanceBase FLOAT,
				@BalanceAdd FLOAT,
				@WalletCodeBase	 NVARCHAR(70),
				@BalanceNext FLOAT

		SELECT 
			@ExpenseBase = [Expense],
			@WalletCodeBase	=  H.[WalletCode]
		FROM [dbo].[SpendingHistory] H
		WHERE [SpendingCode] = @Code
			AND [IsActived] = 1
			AND [UserCode] = @UserCode
		
		SELECT 
			@BalanceBase = [Balance]
		FROM [dbo].[Wallet] W
		WHERE [Code] = @WalletCodeBase
			AND [IsDeleted] = 0
			AND [UserCode] = @UserCode

		SELECT 
			@BalanceNext = [Balance]
		FROM [dbo].[Wallet] W
		WHERE [Code] = @WalletCode
			AND [IsDeleted] = 0
			AND [UserCode] = @UserCode

		BEGIN -- BEGIN VERIFY PARAMETER
			IF @WalletCode = @WalletCodeBase
			BEGIN
				IF (@ExpenseBase + @BalanceBase) < @Expense
				BEGIN
					SET @ErrorMessage = 'The balance code "' + @WalletCode + '" just have "' + CAST((@ExpenseBase + @BalanceBase) AS NVARCHAR) + '". Not enough to payment for this item.'

					RAISERROR(@ErrorMessage,16,1)
				END 
			END
			ELSE 
			BEGIN
				IF @BalanceNext < @Expense
				BEGIN
					SET @ErrorMessage = 'The balance code "' + @WalletCode + '" just have "' + CAST((@BalanceNext) AS NVARCHAR) + '". Not enough to payment for this item.'

					RAISERROR(@ErrorMessage,16,1)
				END 
			END

			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Code,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Spending] WHERE [Code] = @Code AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The code does not exist or has been deleted.'

				IF ISNULL(@Code,'') = ''
				BEGIN
					SET @ErrorMessage = 'The code of spending must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Title,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The title of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@PaymentDate,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The payment date of spending must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Expense,'') = '' OR @Expense <= 0
			BEGIN
				SET @ErrorMessage = 'The expense of spending must not be empty.'

				IF @Expense <= 0
				BEGIN
					SET @ErrorMessage = 'The expense no less than 0.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END  

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END

				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
		
			If OBJECT_ID('tempdb..#TmpWalletHistory') IS NOT NULL
				DROP TABLE #TmpWalletHistory
			SELECT 
				[WalletCode]	
				,[ReferenceCode]
				,@BalanceBase AS [Amount]		
				,[Expense]		
				,@BalanceBase + @ExpenseBase AS [Balance]		
				,[Description]	
				,GETDATE() [CreatedDate]
				,GETDATE() AS [LastModified]
				,'ENTRY' AS [WayCode]		
				,[IsSpending]
				,0 AS [IsActived]
				,[UserCode]
			INTO #TmpWalletHistory
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [UserCode] = @UserCode
				
			UPDATE H
			SET	[IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[WalletHistory] H
			WHERE [ReferenceCode] = @Code
				AND [WalletCode] = @WalletCodeBase
				AND [IsActived] = 1
				AND [IsSpending] = 1
				AND [UserCode] = @UserCode
		
			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[CreatedDate]
				,[LastModified]
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			SELECT 
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[CreatedDate]
				,[LastModified]
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode] 
			FROM #TmpWalletHistory		

			UPDATE H
			SET [IsActived] = 0,
				[LastModified] = GETDATE()
			FROM [dbo].[SpendingHistory] H
			WHERE [SpendingCode] = @Code
				AND [IsActived] = 1
				AND [UserCode] = @UserCode

			INSERT INTO [dbo].[SpendingHistory]
			(
				[WalletCode]		
				,[SpendingCode]		
				,[Expense]				
				,[ActionCode]		
				,[IsActived]	
				,[UserCode]
			)
			VALUES
			(
				@WalletCode,
				@Code,
				@Expense,
				'UPDATED',
				1,
				@UserCode
			)

			UPDATE W
			SET [Balance] = @BalanceBase + @ExpenseBase,
				[LastModified] = GETDATE()
			FROM [dbo].Wallet W
			WHERE [Code] = @WalletCodeBase
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
		
			SELECT 
				@BalanceAdd = [Balance]
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [UserCode] = @UserCode

			INSERT INTO [dbo].[WalletHistory]
			(
				 [WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode	
				,@Code
				,@BalanceAdd		
				,@Expense		
				,@BalanceAdd - @Expense		
				,@Title	
				,'EXIT'		
				,1
				,1
				,@UserCode
			)

			UPDATE W
			SET [Balance] = [Balance] - @Expense,
				[LastModified] = GETDATE()
			FROM [dbo].Wallet W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode

			UPDATE W
			SET [Expense] = @Expense,
				[Title] = @Title,
				[Description] = @Description,
				[PaymentDate] = @PaymentDate,
				[Month] = MONTH(@PaymentDate),
				[Year] = YEAR(@PaymentDate),
				[LastModified] = GETDATE()
			FROM [dbo].[Spending] W
			WHERE [Code] = @Code
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
			
			DROP TABLE #TmpWalletHistory
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_user_SignIn]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.20 14:13 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Created
 --=============================================
CREATE PROC [dbo].[sp_sav_user_SignIn]
(
--DECLARE
	@Username	VARCHAR(500) 	    ,
	@Password	VARCHAR(500) 	    
)AS
BEGIN
	
	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Username] = @Username AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)) = 0 
			BEGIN
				SET @ErrorMessage  = 'The username is (' + @Username + ') cannot exist or password wrong.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER
		
		SELECT	
			[Code]
			,[Username]
			,[FullName]
			,[TokenCode]
			,[Phone]
			,[Role]
		FROM [dbo].[User] R
		WHERE 
			[Username] = @Username
			AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@Username,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_user_SignUp]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.20 14:13 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Created
 --=============================================
CREATE PROC [dbo].[sp_sav_user_SignUp]
(
--DECLARE
	@Code		VARCHAR(100) 	,
	@Username	VARCHAR(500) 	,
	@Password	VARCHAR(500) 	,
	@TokenCode	VARCHAR(500) 	,
	@FullName	NVARCHAR(100)	,
	@Phone		VARCHAR(20)		,
	@Role		VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Code,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The code of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Username,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The username of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Password,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The password of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@TokenCode,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The token of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@FullName,'') = ''  
			BEGIN
				SET @ErrorMessage  = 'The full name of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Phone,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The phone of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Phone,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The phone of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Username] = @Username) > 0 
			BEGIN
				SET @ErrorMessage  = 'The username is (' + @Username + ') cannot be duplicated.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @Code) > 0 
			BEGIN
				SET @ErrorMessage  = 'The code is (' + @Code + ') cannot be duplicated.'
				RAISERROR(@ErrorMessage,16,1)
			END
			
			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [TokenCode] = @TokenCode) > 0 
			BEGIN
				SET @ErrorMessage  = 'The token code is (' + @TokenCode + ') cannot be duplicated.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			INSERT INTO [dbo].[User]
			(
				[Code]		
				,[Username]	
				,[Password]	
				,[TokenCode] 
				,[FullName]  
				,[Phone]
				,[Role]
			)
			VALUES
			(
				@Code		
				,@Username
				,CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)
				,@TokenCode
				,@FullName  
				,@Phone		
				,@Role
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT	
			[Code]
			,[Username]
			,[FullName]
			,[TokenCode]
			,[Phone]
			,[Role]
		FROM [dbo].[User] R
		WHERE 
			[Code] = @Code	
			AND [Username] = @Username
			AND [TokenCode] = @TokenCode
			AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,@Code)
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_user_Update]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.20 14:13 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Created
 --=============================================
CREATE PROC [dbo].[sp_sav_user_Update]
(
--DECLARE
	@Code		VARCHAR(100) 	,
	@Username	VARCHAR(500) 	,
	@Password	VARCHAR(500) 	,
	@TokenCode	VARCHAR(500) 	,
	@FullName	NVARCHAR(100)	,
	@Phone		VARCHAR(20)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Code,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The code of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Username,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The username of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Password,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The password of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@TokenCode,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The token of user imust not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@FullName,'') = ''  
			BEGIN
				SET @ErrorMessage  = 'The full name of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Phone,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The phone of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Username] = @Username AND [Code] <> @Code) > 0 
			BEGIN
				SET @ErrorMessage  = 'The user code already exists.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @Code AND [TokenCode] = @TokenCode AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)) <= 0 
			BEGIN
				SET @ErrorMessage  = 'The password of user wrong.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE u
			SET [Username] = @Username
				,[Fullname] = @FullName
				,[Phone] = @Phone
			FROM [dbo].[User] u
			WHERE
				[Code]	= @Code	
				AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)
				AND [TokenCode] = @TokenCode
		END
		-- END BODY

		COMMIT TRAN 

		SELECT	
			[Code]
			,[Username]
			,[FullName]
			,[TokenCode]
			,[Phone]
			,[Role]
		FROM [dbo].[User] R
		WHERE 
			[Code] = @Code	
			AND [Username] = @Username
			AND [TokenCode] = @TokenCode
			AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @Password),2)

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,@Code)
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_user_UpdatePassword]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.20 14:13 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Created
 --=============================================
CREATE PROC [dbo].[sp_sav_user_UpdatePassword]
(
--DECLARE
	@Code		VARCHAR(100) 	,
	@Username	VARCHAR(500) 	,
	@PasswordOld	VARCHAR(500) 	,
	@PasswordNew	VARCHAR(500) 	,
	@TokenCode	VARCHAR(500) 	
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Code,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The code of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Username,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The username of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@PasswordNew,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The password of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@PasswordOld,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The password of user must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@TokenCode,'') = '' 
			BEGIN
				SET @ErrorMessage  = 'The token of user imust not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Username] = @Username AND [Code] = @Code) = 0 
			BEGIN
				SET @ErrorMessage  = 'The user code doesn''t exists.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @Code AND [TokenCode] = @TokenCode AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @PasswordOld),2)) <= 0 
			BEGIN
				SET @ErrorMessage  = 'The password of user wrong.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END 
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE u
			SET [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @PasswordNew),2)
			FROM [dbo].[User] u
			WHERE
				[Code]	= @Code	
				AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @PasswordOld),2)
				AND [TokenCode] = @TokenCode
		END
		-- END BODY

		COMMIT TRAN 

		SELECT	
			[Code]
			,[Username]
			,[FullName]
			,[TokenCode]
			,[Phone]
			,[Role]
		FROM [dbo].[User] R
		WHERE 
			[Code] = @Code	
			AND [Username] = @Username
			AND [TokenCode] = @TokenCode
			AND [Password] = CONVERT(VARCHAR(400),HashBytes('SHA2_256', @PasswordNew),2)

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,@Code)
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallet_Add]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 10:36 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_Add]
(
--DECLARE
	@WalletCode		VARCHAR(70)			 ,
	@Name			NVARCHAR(50)		 ,
	@Balance		FLOAT				 ,
	@UserCode		VARCHAR(100)		 ,
	@TypeCode		INT
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 
			
			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [UserCode] = @UserCode) > 0
			BEGIN
				SET @ErrorMessage = 'The wallet code is (' + @WalletCode + ') cannot be duplicated.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [IsDeleted] = 0 AND [Name] = @Name AND [UserCode] = @UserCode) > 0
			BEGIN
				SET @ErrorMessage = 'The wallet name is (' + @Name + ') cannot be duplicated.'

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@TypeCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Id] = @TypeCode AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The type wallet code does not exist or has been deleted.'

				IF ISNULL(@TypeCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The type wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Name,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The name wallet must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END 
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			INSERT INTO [dbo].[Wallet] 
			(
				[Code]			
				,[Name]			
				,[Balance]		
				-- ,[IsDefault]		
				,[IsDeleted]		
				,[TypeCode]		
				,[UserCode]		
			)
			VALUES
			(
				@WalletCode			
				,@Name			
				,@Balance		
				-- ,0		
				,0		
				,@TypeCode	
				,@UserCode		
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 
			[Code]	
			,[Name]		
			,[Balance]	
			,[UserCode]	
			,[TypeCode]
			-- ,[IsDefault]
		FROM [dbo].[Wallet] 
		WHERE [Code] = @WalletCode
			AND [UserCode] = @UserCode
			AND [TypeCode] = @TypeCode
			AND [IsDeleted] = 0

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallet_AddMoney]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.18 14:57 PM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_AddMoney]
(
--DECLARE
	@WalletCode		VARCHAR(70)			 ,
	@Amount			FLOAT				 ,
	@Description	NVARCHAR(500)	= NULL,
	@UserCode		VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of spending must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @Amount <= 0 OR ISNULL(@Amount,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The amount of wallet must not be empty.'

				IF @Amount <= 0
				BEGIN
					SET @ErrorMessage = 'The amount no less than 0.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			DECLARE
				@BalanceBase FLOAT

			UPDATE W
			SET W.[Balance] = [Balance] + @Amount,
				@BalanceBase = [Balance],
				[LastModified] = GETDATE()
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode
			
			INSERT INTO [dbo].[WalletHistory]
			(
				 [WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode	
				,@WalletCode
				,@BalanceBase		
				,@Amount		
				,@BalanceBase + @Amount		
				,@Description
				,'ENTRY'		
				,0
				,1
				,@UserCode
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallet_Delete]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_Delete]
(
--DECLARE
	@WalletCode		VARCHAR(70)			 ,
	@UserCode		VARCHAR(100)		 
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of spending must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 
			
			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF (SELECT COUNT(*) FROM [dbo].[WalletHistory] WHERE ([WalletCode] = @WalletCode OR [ReferenceCode] = @WalletCode) AND [UserCode] = @UserCode) <> 0
			BEGIN
				SET @ErrorMessage = 'The wallet code has been reference by others.'
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [UserCode] = @UserCode ) <> 0 
			BEGIN
				SET @ErrorMessage = 'The wallet code can''t deleted.'
				RAISERROR(@ErrorMessage,16,1)
			END 
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE W
			SET W.[IsDeleted] = 1,
				[LastModified] = GETDATE()
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				-- AND [IsDefault] = 0
				AND [UserCode] = @UserCode
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallet_MoveMoney]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.18 11:05 AM>
 --Modified date: <Modified Date,,2019.07.22 09:45 AM>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_MoveMoney]
(
--DECLARE
	@WalletCode			VARCHAR(70)		 ,
	@ReferenceCode		VARCHAR(500)			 ,
	@Description		NVARCHAR(500)	= NULL 	 ,
	@Expense			FLOAT					 ,
	@UserCode			VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF	  
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of spending must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @Expense <= 0 OR ISNULL(@Expense,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The expense of wallet must not be empty.'

				IF @Expense <= 0
				BEGIN
					SET @ErrorMessage = 'The expense no less than 0.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@ReferenceCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @ReferenceCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The reference code does not exist or has been deleted.'

				IF ISNULL(@ReferenceCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The reference code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF (SELECT [Balance] FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) < @Expense
			BEGIN
				SET @ErrorMessage = 'The expense no greate than your balance.'

				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @WalletCode = @ReferenceCode
			BEGIN
				SET @ErrorMessage = 'The source wallet and destination wallet must be different.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			DECLARE 
				@BalanceWalletBase FLOAT,
				@BalanceReferenceBase FLOAT,
				@NameWalletBase NVARCHAR(200),
				@NameReferenceBase NVARCHAR(200)
		
			UPDATE W
			SET @BalanceWalletBase = [Balance],
				@NameWalletBase = [Name],
				[Balance] = [Balance] - @Expense
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode

			UPDATE W
			SET @BalanceReferenceBase = [Balance],
				@NameReferenceBase = [Name],
				[Balance] = [Balance] + @Expense
			FROM [dbo].[Wallet] W
			WHERE [Code] = @ReferenceCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode

			INSERT INTO [dbo].[WalletHistory]
			(
				[WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			SELECT 
				@WalletCode [WalletCode],
				@ReferenceCode [ReferenceCode],
				@BalanceWalletBase [Amount],
				@Expense [Expense],
				@BalanceWalletBase - @Expense [Balance],
				CASE 
					WHEN @Description IS NULL OR @Description = '' THEN 'Move money from wallet "' + @NameWalletBase + '" to "' + @NameReferenceBase + '"'
					ELSE @Description END [Description],
				'EXIT' [WayCode],
				0 [IsSpending],
				1 [IsActived],
				@UserCode
			UNION 
			SELECT 
				@ReferenceCode [WalletCode],
				@WalletCode [ReferenceCode],
				@BalanceReferenceBase [Amount],
				@Expense [Expense],
				@BalanceReferenceBase + @Expense [Balance],
				CASE 
					WHEN @Description IS NULL OR @Description = '' THEN 'Move money from wallet "' + @NameReferenceBase + '" to "' + @NameWalletBase + '"'
					ELSE @Description END [Description],
				'ENTRY' [WayCode],
				0 [IsSpending],
				1 [IsActived],
				@UserCode
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallet_SubMoney]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 10:37 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Done
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_SubMoney]
(
--DECLARE
	@WalletCode		VARCHAR(70)			 ,
	@Amount			FLOAT				 ,
	@Description	NVARCHAR(500)	= NULL,
	@UserCode		VARCHAR(100)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of spending must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF @Amount <= 0 OR ISNULL(@Amount,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The amount of wallet must not be empty.'

				IF @Amount <= 0
				BEGIN
					SET @ErrorMessage = 'The amount no less than 0.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF (SELECT [Balance] FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) < @Amount
			BEGIN
				SET @ErrorMessage = 'The amount no greate than your balance.'

				RAISERROR(@ErrorMessage,16,1)
			END 
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			DECLARE
				@BalanceBase FLOAT

			UPDATE W
			SET W.[Balance] = [Balance] - @Amount,
				@BalanceBase = [Balance],
				[LastModified] = GETDATE()
			FROM [dbo].[Wallet] W
			WHERE [Code] = @WalletCode
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode

			INSERT INTO [dbo].[WalletHistory]
			(
				 [WalletCode]	
				,[ReferenceCode]
				,[Amount]		
				,[Expense]		
				,[Balance]		
				,[Description]	
				,[WayCode]		
				,[IsSpending]
				,[IsActived]
				,[UserCode]
			)
			VALUES
			(
				@WalletCode	
				,@WalletCode
				,@BalanceBase		
				,@Amount		
				,@BalanceBase - @Amount		
				,@Description
				,'EXIT'		
				,0
				,1
				,@UserCode
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallet_Update]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallet_Update]
(
--DECLARE
	@WalletCode		VARCHAR(70)			 ,
	@Name			NVARCHAR(50)		 ,
	@UserCode		VARCHAR(100)		 ,
	@TypeCode		INT
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The user code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@WalletCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [Code] = @WalletCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The wallet code does not exist or has been deleted.'

				IF ISNULL(@WalletCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@TypeCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Id] = @TypeCode AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage = 'The type wallet code does not exist or has been deleted.'

				IF ISNULL(@TypeCode,'') = ''
				BEGIN
					SET @ErrorMessage = 'The type wallet code of wallet must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END 

			IF ISNULL(@Name,'') = '' 
			BEGIN
				SET @ErrorMessage = 'The name wallet must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE [dbo].[Wallet] 
			SET	[Name]	= @Name	
				,[TypeCode] = @TypeCode
				,[LastModified] = GETDATE()
			FROM [dbo].[Wallet] 
			WHERE 
				[Code] = @WalletCode	
				AND [IsDeleted] = 0
				AND [UserCode] = @UserCode	
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallettype_Add]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallettype_Add]
(
--DECLARE
	@Key		NVARCHAR(70),
	@Name		NVARCHAR(500),
	@UserCode	VARCHAR(400)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Key,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Key] = UPPER(@Key) AND [IsDeleted] = 0 AND [UserCode] = @UserCode) > 0 
			BEGIN
				SET @ErrorMessage  = 'The code is (' + @Key + ') cannot be duplicated.'

				IF ISNULL(@Key,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The key of wallet type must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Name,'') = ''
			BEGIN
				SET @ErrorMessage  = 'The name of wallet type must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage  = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The user code must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			INSERT INTO [dbo].[WalletType]
			(
				[Key]		
				,[Name]		
				,[IsDeleted] 
				-- ,[IsDefault] 
				,[UserCode]
			)
			VALUES
			(
				UPPER(@Key)		
				,@Name		
				-- ,0 
				,0 
				,@UserCode
			)
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 
			[Id],
			[Key],
			[Name],
			-- [IsDefault],
			[IsDeleted],
			[UserCode]
		FROM [dbo].[WalletType]
		WHERE [Key] = @Key
			AND [Name] = @Name
			AND [IsDeleted] = 0
			AND [UserCode] = @UserCode

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallettype_Delete]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallettype_Delete]
(
--DECLARE
	@Id			INT,
	@UserCode	VARCHAR(400)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Id,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Id] = @Id AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0 
			BEGIN
				SET @ErrorMessage  = 'The key does not exist or has been deleted.'

				IF ISNULL(@Id,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The key of wallet type must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage  = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The user code must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[Wallet] WHERE [TypeCode] = @Id AND [IsDeleted] = 0 AND [UserCode] = @UserCode) <> 0 
			BEGIN
				SET @ErrorMessage  = 'The type code has been reference by others.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE T
			SET T.[IsDeleted] = 1,
				T.[LastModified] = GETDATE()
			FROM [dbo].[WalletType] T
			WHERE
				[UserCode] = @UserCode
				--AND [IsDefault] = 0
				AND [IsDeleted] = 0
				AND [Id] = @Id
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,ISNULL(@UserCode,''))
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sp_sav_wallettype_Update]    Script Date: 11/25/2019 10:14:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=============================================
 --Author:		<Author,,Nguyen Truong Vu>
 --Created date: <Created Date,,2019.07.22 11:15 AM>
 --Modified date: <Modified Date,,>
 --Description:	<Description,,>
 --Status: Create
 --=============================================
CREATE PROC [dbo].[sp_sav_wallettype_Update]
(
--DECLARE
	@Id			INT,
	@Key		NVARCHAR(70),
	@Name		NVARCHAR(500),
	@UserCode	VARCHAR(400)
)AS
BEGIN

	SET NOCOUNT ON

	SET FMTONLY OFF
	
	DECLARE 
		@ErrorMessage NVARCHAR(4000)

	BEGIN TRY  
		BEGIN TRAN -- BEGIN TRANSACTION
		
		BEGIN -- BEGIN VERIFY PARAMETER
			IF ISNULL(@Key,'') = ''
			BEGIN
				SET @ErrorMessage  = 'The key of wallet type must not be empty.'
				RAISERROR(@ErrorMessage,16,1)
			END
				
			IF ISNULL(@Id,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Id] = @Id AND [IsDeleted] = 0 AND [UserCode] = @UserCode) = 0 
			BEGIN
				SET @ErrorMessage  = 'The id does not exist or has been deleted.'

				IF ISNULL(@Key,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The id of wallet type must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@Key,'') = '' OR (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Key] = UPPER(@Key) AND [IsDeleted] = 0 AND [UserCode] = @UserCode) > 0 
			BEGIN
				SET @ErrorMessage  = 'The key does exist or has been deleted.'

				IF ISNULL(@Key,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The key of wallet type must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF ISNULL(@UserCode,'') = '' OR (SELECT COUNT(*) FROM [dbo].[User] WHERE [Code] = @UserCode) = 0
			BEGIN
				SET @ErrorMessage  = 'The user code does not exist or has been deleted.'

				IF ISNULL(@UserCode,'') = ''
				BEGIN
					SET @ErrorMessage  = 'The user code must not be empty.'
				END
				RAISERROR(@ErrorMessage,16,1)
			END

			IF (SELECT COUNT(*) FROM [dbo].[WalletType] WHERE [Id] = @Id AND [UserCode] = @UserCode ) <> 0 
			BEGIN
				SET @ErrorMessage = 'The wallet type can''t deleted.'
				RAISERROR(@ErrorMessage,16,1)
			END
		END
		-- END VERIFY PARAMETER

		BEGIN -- BEGIN BODY
			UPDATE T
			SET T.[Name] = @Name,
				T.[LastModified] = GETDATE(),
				[Key] = UPPER(@Key)
			FROM [dbo].[WalletType] T
			WHERE
				[UserCode] = @UserCode
				-- AND [IsDefault] = 0
				AND [IsDeleted] = 0
				AND [Id] = @Id
				
		END
		-- END BODY

		COMMIT TRAN 

		SELECT 1 AS [Value]

	END TRY  
	BEGIN CATCH  
		ROLLBACK TRAN

		DECLARE 
			@NameStore NVARCHAR(500)
		SELECT @NameStore = OBJECT_NAME(@@PROCID)

		IF(@ErrorMessage IS NULL)
		BEGIN
			SELECT @ErrorMessage = ERROR_MESSAGE()
		END

		INSERT INTO [Logger]([Method],[Message],[UserCode]) VALUES ('EXEC [' + @NameStore + ']',@ErrorMessage,@UserCode)
		
		RAISERROR(@ErrorMessage,16,1)
	END CATCH
END
GO
USE [master]
GO
ALTER DATABASE [SAVDB] SET  READ_WRITE 
GO
