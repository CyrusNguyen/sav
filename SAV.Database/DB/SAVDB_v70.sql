USE [master]
GO

CREATE DATABASE [SAVDB]

GO

USE [SAVDB]

GO

CREATE TABLE [dbo].[Spending]
(      
	[Id]        	INT IDENTITY 	PRIMARY KEY					NOT NULL					,
	[Code]			VARCHAR(MAX)	                 			NOT NULL					,
	[Title]			NVARCHAR(100)								NOT NULL                   	,
	[Description]	NVARCHAR(2000)								NULL                       	,
	[Expense]		FLOAT										NOT NULL                   	,
	[PaymentDate]	DATETIME									NOT NULL                   	,
	[Month]			INT											DEFAULT MONTH(GETDATE())	,	            
	[Year]			INT											DEFAULT YEAR(GETDATE())		,	            
	[CreatedDate]	DATETIME									DEFAULT GETDATE()			,	            
	[LastModified]	DATETIME									DEFAULT GETDATE()			,	            
	[IsDeleted]		BIT											NOT NULL                   	,
	[UserCode]		VARCHAR(MAX)								NOT NULL                  	
)

GO

CREATE TABLE [dbo].[Wallet]
(
	[Id]        	INT IDENTITY 	PRIMARY KEY					NOT NULL				,
	[Code]			VARCHAR(MAX)								NOT NULL	  			,
	[Name]			NVARCHAR(50)								NOT NULL                ,       
	[Balance]		FLOAT										NOT NULL                ,       
	[CreatedDate]	DATETIME									DEFAULT GETDATE()   	,	    		
	[LastModified]	DATETIME									DEFAULT GETDATE()   	,	    		
	-- [IsDefault]		BIT											NOT NULL                ,       
	[IsDeleted]		BIT											NOT NULL                ,       
	[TypeCode]		INT			 								NULL                    ,       
	[UserCode]		VARCHAR(MAX)								NOT NULL                      
)

GO

CREATE TABLE [dbo].[WalletType]
(
	[Id]        	INT IDENTITY 	PRIMARY KEY						NOT NULL			,			
	[Key]			VARCHAR(MAX) 									NOT NULL			,			
	[Name]			NVARCHAR(500)									NOT NULL			,			
	[IsDeleted] 	BIT												NOT NULL			,	
	[CreatedDate]	DATETIME										DEFAULT GETDATE()   ,	    		
	[LastModified]	DATETIME										DEFAULT GETDATE()   ,
	-- [IsDefault] 	BIT												NOT NULL			,			
	[UserCode]		VARCHAR(MAX)									NOT NULL
)

GO

CREATE TABLE [dbo].[SpendingHistory]
(
	[Id]				INT IDENTITY	PRIMARY KEY					NOT NULL			,
	[WalletCode]		VARCHAR(MAX)									NOT NULL			,
	[SpendingCode]		VARCHAR(MAX)									NOT NULL	 		,
	[Expense]			FLOAT 										NOT NULL			,
	[CreatedDate]		DATETIME									DEFAULT GETDATE()	,					
	[LastModified]		DATETIME									DEFAULT GETDATE()	,					
	[ActionCode]		VARCHAR(20)									NOT NULL			,
	[IsActived]			BIT											NOT NULL			,
	[UserCode]			VARCHAR(MAX)								NOT NULL			,
)

GO

CREATE TABLE [dbo].[WalletHistory]
(
	[Id]				INT IDENTITY	 PRIMARY KEY		NOT NULL			,
	[WalletCode]		VARCHAR(MAX)							NOT NULL			,
	[ReferenceCode]		VARCHAR(MAX)						NOT NULL			,
	[Amount]			FLOAT								NOT NULL			,
	[Expense]			FLOAT								NOT NULL			,
	[Balance]			FLOAT								NOT NULL			,
	[Description]		NVARCHAR(2000)						NULL				,
	[CreatedDate]		DATETIME							DEFAULT GETDATE()	,	
	[LastModified]		DATETIME							DEFAULT GETDATE()	,	
	[WayCode]			VARCHAR(20)							NOT NULL			,
	[IsSpending]		BIT									NOT NULL			,
	[IsActived]			BIT									NOT NULL			,
	[UserCode]			VARCHAR(MAX)                        NOT NULL
)

GO

CREATE TABLE [Logger]
(
	[Id]				INT IDENTITY		PRIMARY KEY			NOT NULL			,
	[Method]			NVARCHAR(500)							NULL				,
	[Message]			NVARCHAR(4000)							NULL				,
	[CreatedDate]		DATETIME								DEFAULT GETDATE()	,
	[LastModified]		DATETIME								DEFAULT GETDATE()	,
	[UserCode]			VARCHAR(100)							NULL
)

GO

CREATE TABLE [User]
(
	[Code]		VARCHAR(100)		PRIMARY KEY		NOT NULL,
	[Username]	VARCHAR(500) 						NOT NULL,
	[Password]	VARCHAR(500) 						NOT NULL,
	[TokenCode] VARCHAR(500) 						NOT NULL,
	[Role]		VARCHAR(100) 						NOT NULL,
	[FullName]  NVARCHAR(100)						NOT NULL,
	[Phone]		VARCHAR(20)							NULL
)

GO

GO

USE [master]
/*
[WayCode]:
	ENTRY (+),
	EXIT (-)
[ActionCode]:
	CREATED
	UPDATED
*/