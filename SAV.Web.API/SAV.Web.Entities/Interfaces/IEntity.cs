﻿namespace SAV.Web.Entities.Interfaces
{
    /// <summary>
    /// Represents for entity interfaces.
    /// </summary>
    public interface IEntity { }
}