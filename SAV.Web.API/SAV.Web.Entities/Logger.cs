namespace SAV.Web.Entities
{
    using SAV.Web.Entities.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    ///
    /// </summary>
    [Table("Logger")]
    public partial class Logger : IEntity
    {
        /// <summary>
        ///
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        ///
        /// </summary>
        [StringLength(500)]
        public string Method { get; set; }

        /// <summary>
        ///
        /// </summary>
        [StringLength(4000)]
        public string Message { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime? LastModified { get; set; }

        /// <summary>
        ///
        /// </summary>
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}