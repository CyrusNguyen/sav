namespace SAV.Web.Entities
{
    using SAV.Web.Entities.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Wallet")]
    public partial class Wallet : IEntity
    {
        [Key]
        public int? Id { get; set; }

        [Required]
        [StringLength(70)]
        public string Code { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public double? Balance { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        //public bool? IsDefault { get; set; } = false;

        public bool IsDeleted { get; set; } = false;

        public int TypeCode { get; set; }

        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}