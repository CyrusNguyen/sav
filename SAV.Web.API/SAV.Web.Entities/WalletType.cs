namespace SAV.Web.Entities
{
    using SAV.Web.Entities.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WalletType")]
    public partial class WalletType : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(90)]
        public string Key { get; set; }

        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        //public bool IsDefault { get; set; }

        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}