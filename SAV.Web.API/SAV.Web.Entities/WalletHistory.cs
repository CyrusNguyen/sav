namespace SAV.Web.Entities
{
    using SAV.Web.Entities.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("WalletHistory")]
    public partial class WalletHistory : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(70)]
        public string WalletCode { get; set; }

        [Required]
        [StringLength(500)]
        public string ReferenceCode { get; set; }

        public double Amount { get; set; }

        public double Expense { get; set; }

        public double Balance { get; set; }

        [StringLength(2000)]
        public string Description { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        [Required]
        [StringLength(20)]
        public string WayCode { get; set; }

        public bool IsSpending { get; set; }

        public bool IsActived { get; set; }

        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}