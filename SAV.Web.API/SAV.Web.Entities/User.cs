namespace SAV.Web.Entities
{
    using SAV.Web.Entities.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("User")]
    public partial class User : IEntity
    {
        [Key]
        [StringLength(100)]
        public string Code { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [MinLength(6)]
        [StringLength(500)]
        public string Username { get; set; }

        [Required]
        [StringLength(500)]
        [MinLength(8)]
        public string Password { get; set; }

        [Required]
        [StringLength(500)]
        public string TokenCode { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(100)]
        public string FullName { get; set; }

        [Required]
        [StringLength(100)]
        public string Role { get; set; }

        [Required]
        [MaxLength(11)]
        [MinLength(10)]
        [StringLength(20)]
        [RegularExpression("([0-9]+)")]
        public string Phone { get; set; }
    }
}