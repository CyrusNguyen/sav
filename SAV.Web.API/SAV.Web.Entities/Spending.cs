namespace SAV.Web.Entities
{
    using SAV.Web.Entities.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Spending")]
    public partial class Spending : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(80)]
        public string Code { get; set; } = Guid.NewGuid().ToString();

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(2000)]
        public string Description { get; set; }

        public double Expense { get; set; }

        public DateTime PaymentDate { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        public bool IsDeleted { get; set; }

        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}