﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Commons.DataAnnotations.Models
{
    /// <summary>
    ///
    /// </summary>
    public class ModelValue
    {
        /// <summary>
        ///
        /// </summary>
        private readonly bool _isValid;

        /// <summary>
        ///
        /// </summary>
        public bool IsValid { get => _isValid; }

        /// <summary>
        ///
        /// </summary>
        private readonly IList<ValidationResult> _errors;

        /// <summary>
        ///
        /// </summary>
        public IList<ValidationResult> Errors { get => _errors; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="isValid"></param>
        /// <param name="errors"></param>
        public ModelValue(bool isValid, IList<ValidationResult> errors)
        {
            _isValid = isValid;
            _errors = errors;
        }
    }
}