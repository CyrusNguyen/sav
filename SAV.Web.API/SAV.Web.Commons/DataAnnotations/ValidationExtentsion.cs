﻿using log4net;
using SAV.Web.Commons.DataAnnotations.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Commons.DataAnnotations
{
    public static class ValidationExtentsion
    {
        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(ValidationExtentsion));

        /// <summary>
        /// Verify fields for objects.
        /// </summary>
        /// <param name="entity">Represent for objects.</param>
        /// <returns>Returns status check validate for entity</returns>
        public static ModelValue ModelState(this object entity)
        {
            try
            {
                var isvalid = false;
                var errors = new List<ValidationResult>();

                if (entity != null)
                {
                    var validContext = new ValidationContext(entity, serviceProvider: null, items: null);
                    isvalid = Validator.TryValidateObject(entity, validContext, errors, validateAllProperties: true);
                }

                return new ModelValue(isvalid, errors);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }
    }
}