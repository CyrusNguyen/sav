﻿using log4net;
using SAV.Web.Commons.Jsons;
using System;
using System.Collections;

namespace SAV.Web.Commons.Mappings
{
    /// <summary>
    ///
    /// </summary>
    public static class MappingExtention
    {
        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MappingExtention));

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="tobject"></param>
        /// <returns></returns>
        public static TObject Mapping<TObject>(this object tobject)
        {
            try
            {
                if (tobject == null)
                {
                    return default;
                }

                return tobject.ToJObject().ToObject<TObject>();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="tobject"></param>
        /// <returns></returns>
        public static TEnumerable MappingList<TEnumerable>(this IEnumerable toList)
        {
            try
            {
                if (toList == null)
                {
                    return default;
                }

                return toList.ToJArray().ToObject<TEnumerable>();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }
    }
}