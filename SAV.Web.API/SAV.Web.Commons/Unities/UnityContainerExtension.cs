﻿using log4net;
using System;
using Unity;
using Unity.Injection;

namespace SAV.Web.Commons.Unities
{
    /// <summary>
    /// The unity helper using support for unity container.
    /// </summary>
    public static class UnityContainerExtension
    {
        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(UnityContainerExtension));

        /// <summary>
        /// The injection constructor with represent is name.
        /// </summary>
        /// <typeparam name="T">Represent for a service type.</typeparam>
        /// <param name="container">Represents for unity container.</param>
        /// <param name="name">Represents for name of a instance inject.</param>
        /// <returns>Return a injection constructor.</returns>
        public static InjectionConstructor InjectionConstructor<T>(this IUnityContainer container, string name)
        {
            try
            {
                return new InjectionConstructor(container.Resolve(typeof(T), name));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The injection constructor with arguments.
        /// </summary>
        /// <param name="container">Represents for unity container.</param>
        /// <param name="arguments">Represents for instance, object inject in the constructor.</param>
        /// <returns>Return a injection constructor.</returns>
        public static InjectionConstructor InjectionConstructor(this IUnityContainer container, params object[] arguments)
        {
            try
            {
                return new InjectionConstructor(arguments);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The register constructor replace for register type.
        /// </summary>
        /// <typeparam name="TFrom">Represents for interface injection.</typeparam>
        /// <typeparam name="TTo">Represents for instance inherit to interface.</typeparam>
        /// <param name="container">Represents for unity container.</param>
        /// <param name="name">Represents for name to add new into the container.</param>
        /// <param name="arguments">Represents for instance, object inject in the constructor.</param>
        public static void RegisterConstructor<TFrom, TTo>(this IUnityContainer container, string name, params object[] arguments) where TTo : TFrom
        {
            try
            {
                container.RegisterType<TFrom, TTo>(name, container.InjectionConstructor(arguments));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The register constructor replace for register type.
        /// </summary>
        /// <typeparam name="IInstance">Represents for instance you want call.</typeparam>
        /// <typeparam name="TInject">Represents for a type has been register in the container is inject of instance.</typeparam>
        /// <param name="container">Represents for unity container.</param>
        /// <param name="injectName">Represents for name of a instance has been register in the container.</param>
        /// <param name="arguments">Represents for instance, object inject in the constructor.</param>
        public static void RegisterInstance<IInstance, TInject>(this IUnityContainer container, string injectName, object log = null)
        {
            try
            {
                container.RegisterType<IInstance>(new InjectionConstructor(new object[] { container.Resolve(typeof(TInject), injectName), log }));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The register constructor replace for register type.
        /// </summary>
        /// <typeparam name="TFrom">Represents for interface injection.</typeparam>
        /// <typeparam name="TTo">Represents for instance inherit to interface.</typeparam>
        /// <typeparam name="TInject">Represents for a type has been register in the container.</typeparam>
        /// <param name="container">Represents for unity container.</param>
        /// <param name="name">Represents for name to add new into the container.</param>
        /// <param name="injectName">Represents for name of a instance has been register in the container.</param>
        public static void RegisterConstructor<TFrom, TTo, TInject>(this IUnityContainer container, string name, string injectName, object log = null) where TTo : TFrom
        {
            try
            {
                container.RegisterType<TFrom, TTo>(name, new InjectionConstructor(new object[] { container.Resolve(typeof(TInject), injectName), log }));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }
    }
}