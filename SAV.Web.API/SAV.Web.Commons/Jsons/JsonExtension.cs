﻿using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;

namespace SAV.Web.Commons.Jsons
{
    /// <summary>
    ///
    /// </summary>
    public static class JsonExtension
    {
        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(JsonExtension));

        /// <summary>
        ///
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static string ToJsonString(this object obj)
        {
            try
            {
                if (obj == null)
                {
                    return default;
                }

                return obj.ToJObject().ToString();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static JObject ToJObject(this object obj)
        {
            try
            {
                if (obj == null)
                {
                    return default;
                }

                return JObject.FromObject(obj, new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static JArray ToJArray(this IEnumerable obj)
        {
            try
            {
                if (obj == null)
                {
                    return default;
                }

                return JArray.FromObject(obj, new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }
    }
}