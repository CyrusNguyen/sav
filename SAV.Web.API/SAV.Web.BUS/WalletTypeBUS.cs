﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DTO.Commons;
using SAV.Web.DTO.WalletType;

namespace SAV.Web.BUS
{
    /// <summary>
    ///
    /// </summary>
    public class WalletTypeBUS : BaseBUS<IWalletTypeDAO>, IWalletTypeBUS
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="baseDAO"></param>
        /// <param name="log"></param>
        public WalletTypeBUS(IWalletTypeDAO baseDAO, ILog log) : base(baseDAO, log)
        {
        }

        #region Get

        public WalletTypeResultDTO[] GetWalletType(string username) => _baseDAO.GetWalletType(username);

        #endregion Get

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        public WalletTypeResultDTO Add(WalletTypeParameterDTO walletTypeDTO) => _baseDAO.Add(walletTypeDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        public ResultDTO Delete(WalletTypeParameterDTO walletTypeDTO) => _baseDAO.Delete(walletTypeDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        public ResultDTO Update(WalletTypeParameterDTO walletTypeDTO) => _baseDAO.Update(walletTypeDTO);
    }
}