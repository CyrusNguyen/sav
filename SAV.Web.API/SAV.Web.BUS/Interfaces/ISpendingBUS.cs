﻿using SAV.Web.DTO.Commons;
using SAV.Web.DTO.Spending;

namespace SAV.Web.BUS.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface ISpendingBUS : IBaseBUS
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        SpendingResultDTO InsertDaily(SpendingParameterDTO spendingParameterDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        ResultDTO UpdateDaily(SpendingParameterDTO spendingParameterDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        ResultDTO DeleteDaily(SpendingParameterDTO spendingParameterDTO);
    }
}