﻿using SAV.Web.DTO.Commons;
using SAV.Web.DTO.Wallet;

namespace SAV.Web.BUS.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IWalletBUS : IBaseBUS
    {
        WalletCommonDTO[] GetWallet(string username);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        WalletResultDTO Add(WalletParameterDTO walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        ResultDTO Update(WalletParameterDTO walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        ResultDTO Delete(WalletParameterDTO walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        ResultDTO MoveMoney(WalletParameterDTO walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        ResultDTO AddMoney(WalletParameterDTO walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        ResultDTO SubMoney(WalletParameterDTO walletDTO);
    }
}