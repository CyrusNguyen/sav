﻿using SAV.Web.DTO.Commons;
using SAV.Web.DTO.WalletType;

namespace SAV.Web.BUS.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IWalletTypeBUS : IBaseBUS
    {
        #region Get

        WalletTypeResultDTO[] GetWalletType(string username);

        #endregion Get



        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        WalletTypeResultDTO Add(WalletTypeParameterDTO walletTypeDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        ResultDTO Update(WalletTypeParameterDTO walletTypeDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        ResultDTO Delete(WalletTypeParameterDTO walletTypeDTO);
    }
}