﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DTO.User;

namespace SAV.Web.BUS
{
    /// <summary>
    ///
    /// </summary>
    public class UserBUS : BaseBUS<IUserDAO>, IUserBUS
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="baseDAO"></param>
        /// <param name="log"></param>
        public UserBUS(IUserDAO baseDAO, ILog log) : base(baseDAO, log)
        {

        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserResultDTO SignUp(UserParameterDTO userDTO) => _baseDAO.SignUp(userDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserResultDTO SignIn(UserParameterDTO userLoginDTO) => _baseDAO.SignIn(userLoginDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserResultDTO Update(UserParameterDTO userDTO) => _baseDAO.Update(userDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserResultDTO UpdatePassword(UserParameterDTO userDTO) => _baseDAO.UpdatePassword(userDTO);
    }
}