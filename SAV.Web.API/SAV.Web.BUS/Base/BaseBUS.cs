﻿using log4net;
using SAV.Web.BUS.Interfaces;

namespace SAV.Web.BUS
{
    /// <summary>
    ///
    /// </summary>
    public class BaseBUS<TBaseDAO> : IBaseBUS
    {
        /// <summary>
        ///
        /// </summary>
        protected readonly TBaseDAO _baseDAO;

        /// <summary>
        ///
        /// </summary>
        protected readonly ILog _log;

        /// <summary>
        ///
        /// </summary>
        /// <param name="baseDAO"></param>
        public BaseBUS(TBaseDAO baseDAO, ILog log)
        {
            _log = log ?? throw new System.ArgumentNullException(nameof(log));
            _baseDAO = baseDAO;
        }
    }
}