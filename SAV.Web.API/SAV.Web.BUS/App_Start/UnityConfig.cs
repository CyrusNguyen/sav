﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.Commons.Unities;
using SAV.Web.DAO.Interfaces;
using Unity;

namespace SAV.Web.BUS
{
    /// <summary>
    /// Represents for unity config of business layer.
    /// </summary>
    public class UnityConfig
    {
        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(UnityConfig));

        /// <summary>
        /// The set unity container with ref type.
        /// </summary>
        /// <param name="container">Represent for unity container root to web layer.</param>
        public static void SetContainers(ref IUnityContainer container)
        {
            // Config container for DAO layer.
            DAO.UnityConfig.SetContainers(ref container);

            // Config container for BUS layer.
            container.RegisterConstructor<ISpendingBUS, SpendingBUS, ISpendingDAO>("SpendingBUS", "SpendingDAO", LogManager.GetLogger(typeof(SpendingBUS)));
            container.RegisterConstructor<IWalletBUS, WalletBUS, IWalletDAO>("WalletBUS", "WalletDAO", LogManager.GetLogger(typeof(WalletBUS)));
            container.RegisterConstructor<IWalletTypeBUS, WalletTypeBUS, IWalletTypeDAO>("WalletTypeBUS", "WalletTypeDAO", LogManager.GetLogger(typeof(WalletTypeBUS)));
            container.RegisterConstructor<IUserBUS, UserBUS, IUserDAO>("UserBUS", "UserDAO", LogManager.GetLogger(typeof(UserBUS)));
        }
    }
}