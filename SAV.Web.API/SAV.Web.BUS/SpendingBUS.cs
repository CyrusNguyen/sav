﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DTO.Commons;
using SAV.Web.DTO.Spending;

namespace SAV.Web.BUS
{
    public class SpendingBUS : BaseBUS<ISpendingDAO>, ISpendingBUS
    {
        public SpendingBUS(ISpendingDAO baseDAO, ILog log) : base(baseDAO, log)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        public ResultDTO DeleteDaily(SpendingParameterDTO spendingParameterDTO) => _baseDAO.DeleteDaily(spendingParameterDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        public SpendingResultDTO InsertDaily(SpendingParameterDTO spendingParameterDTO) => _baseDAO.InsertDaily(spendingParameterDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        public ResultDTO UpdateDaily(SpendingParameterDTO spendingParameterDTO) => _baseDAO.UpdateDaily(spendingParameterDTO);
    }
}