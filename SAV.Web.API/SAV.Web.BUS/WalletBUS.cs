﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DTO.Commons;
using SAV.Web.DTO.Wallet;

namespace SAV.Web.BUS
{
    /// <summary>
    ///
    /// </summary>
    public class WalletBUS : BaseBUS<IWalletDAO>, IWalletBUS
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="baseDAO"></param>
        /// <param name="log"></param>
        public WalletBUS(IWalletDAO baseDAO, ILog log) : base(baseDAO, log)
        {
        }

        public WalletCommonDTO[] GetWallet(string username) => _baseDAO.GetWallet(username);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        public WalletResultDTO Add(WalletParameterDTO walletDTO) => _baseDAO.Add(walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        public ResultDTO AddMoney(WalletParameterDTO walletDTO) => _baseDAO.AddMoney(walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        public ResultDTO Delete(WalletParameterDTO walletDTO) => _baseDAO.Delete(walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        public ResultDTO MoveMoney(WalletParameterDTO walletDTO) => _baseDAO.MoveMoney(walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        public ResultDTO SubMoney(WalletParameterDTO walletDTO) => _baseDAO.SubMoney(walletDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDTO"></param>
        /// <returns></returns>
        public ResultDTO Update(WalletParameterDTO walletDTO) => _baseDAO.Update(walletDTO);
    }
}