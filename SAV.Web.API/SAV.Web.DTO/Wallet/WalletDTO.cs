namespace SAV.Web.DTO.Wallet
{
    using SAV.Web.DTO.Interfaces;
    using System;

    public partial class WalletDTO : IEntityDTO
    {
        public int? Id { get; set; }

        public string Code { get; set; } = Guid.NewGuid().ToString();

        public string WalletCode { get => Code; set => Code = value; }

        public string Name { get; set; }

        public double? Balance { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        //public bool? IsDefault { get; set; }

        public bool? IsDeleted { get; set; }

        public int? TypeCode { get; set; }

        public string UserCode { get; set; }
    }
}