namespace SAV.Web.DTO.Wallet
{
    using SAV.Web.DTO.Interfaces;
    using System;

    public partial class WalletParameterDTO : IEntityDTO
    {
        public int? Id { get; set; }

        public string Code { get; set; } = Guid.NewGuid().ToString();

        public string WalletCode { get => Code; set => Code = value; }

        public string ReferenceCode { get; set; }

        public int? TypeCode { get; set; }

        public string UserCode { get; set; }

        public string Name { get; set; }

        public double? Balance { get; set; }

        public double? Expense { get; set; }

        public double? Amount { get; set; }

        //public bool? IsDefault { get; set; }

        public bool? IsDeleted { get; set; }

        public string Description { get; set; }
    }
}