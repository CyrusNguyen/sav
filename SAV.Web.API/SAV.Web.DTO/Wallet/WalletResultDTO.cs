namespace SAV.Web.DTO.Wallet
{
    using SAV.Web.DTO.Interfaces;

    public partial class WalletResultDTO : IEntityDTO
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public double? Balance { get; set; }

        //public bool? IsDefault { get; set; }

        public int? TypeCode { get; set; }

        public string UserCode { get; set; }
    }
}