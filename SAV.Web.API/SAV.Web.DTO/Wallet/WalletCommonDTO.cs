namespace SAV.Web.DTO.Wallet
{
    public partial class WalletCommonDTO : WalletResultDTO
    {
        public int TypeId { get; set; }

        public string TypeKey { get; set; }

        public string TypeName { get; set; }

        public bool IsDeleted { get; set; }
    }
}