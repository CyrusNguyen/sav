namespace SAV.Web.DTO
{
    using SAV.Web.DTO.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class SpendingHistoryDTO : IEntityDTO
    {
        public int? Id { get; set; }

        [Required]
        [StringLength(70)]
        public string WalletCode { get; set; }

        [Required]
        [StringLength(80)]
        public string SpendingCode { get; set; }

        public double? Expense { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        [Required]
        [StringLength(20)]
        public string ActionCode { get; set; }

        public bool? IsActived { get; set; }

        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}