namespace SAV.Web.DTO.WalletType
{
    using SAV.Web.DTO.Interfaces;

    public partial class WalletTypeResultDTO : IEntityDTO
    {
        public int? Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public bool? IsDeleted { get; set; }

        //public bool? IsDefault { get; set; }

        public string UserCode { get; set; }
    }
}