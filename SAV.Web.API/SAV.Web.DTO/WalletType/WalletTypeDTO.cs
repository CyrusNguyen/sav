namespace SAV.Web.DTO.WalletType
{
    using SAV.Web.DTO.Interfaces;
    using System;

    public partial class WalletTypeDTO : IEntityDTO
    {
        public int? Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public bool? IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        //public bool? IsDefault { get; set; }

        public string UserCode { get; set; }
    }
}