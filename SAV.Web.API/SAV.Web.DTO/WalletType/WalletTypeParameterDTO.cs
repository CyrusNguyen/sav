namespace SAV.Web.DTO.WalletType
{
    using SAV.Web.DTO.Interfaces;

    public partial class WalletTypeParameterDTO : IEntityDTO
    {
        public int? Id { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string UserCode { get; set; }
    }
}