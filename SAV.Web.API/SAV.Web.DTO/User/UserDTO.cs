using SAV.Web.DTO.Interfaces;
using System;

namespace SAV.Web.DTO.User
{
    public partial class UserDTO : IEntityDTO
    {
        public string Code { get; set; } = Guid.NewGuid().ToString();

        public string UserCode { get => Code; set => Code = value; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string TokenCode { get; set; } = Guid.NewGuid().ToString();

        public string FullName { get; set; }

        public string Role { get; set; }

        public string Phone { get; set; }
    }
}