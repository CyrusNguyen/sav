﻿using SAV.Web.DTO.Interfaces;

namespace SAV.Web.DTO.User
{
    public class UserResultDTO : IEntityDTO
    {
        public string Code { get; set; }

        public string Username { get; set; }

        public string TokenCode { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Role { get; set; }
    }
}