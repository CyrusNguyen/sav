namespace SAV.Web.DTO
{
    using SAV.Web.DTO.Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class LoggerDTO : IEntityDTO
    {
        public int? Id { get; set; }

        [StringLength(500)]
        public string Method { get; set; }

        [StringLength(4000)]
        public string Message { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        [StringLength(100)]
        public string UserCode { get; set; }
    }
}