namespace SAV.Web.DTO.Spending
{
    using SAV.Web.DTO.Interfaces;
    using System;

    public partial class SpendingDTO : IEntityDTO
    {
        public int? Id { get; set; }

        public string Code { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public double? Expense { get; set; }

        public DateTime? PaymentDate { get; set; }

        public int? Month { get; set; }

        public int? Year { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModified { get; set; }

        public bool? IsDeleted { get; set; }

        public string UserCode { get; set; }
    }
}