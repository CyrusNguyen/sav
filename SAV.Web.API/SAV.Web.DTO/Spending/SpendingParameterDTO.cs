namespace SAV.Web.DTO.Spending
{
    using SAV.Web.DTO.Interfaces;
    using System;

    public partial class SpendingParameterDTO : IEntityDTO
    {
        public string Code { get; set; } = Guid.NewGuid().ToString();

        public string SpedingCode { get => Code; set => Code = value; }

        public string Title { get; set; }

        public string Description { get; set; }

        public double? Expense { get; set; }

        public DateTime? PaymentDate { get; set; }

        public string UserCode { get; set; }

        public string WalletCode { get; set; }
    }
}