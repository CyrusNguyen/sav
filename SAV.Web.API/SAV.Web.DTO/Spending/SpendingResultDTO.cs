namespace SAV.Web.DTO.Spending
{
    using SAV.Web.DTO.Interfaces;
    using System;

    public partial class SpendingResultDTO : IEntityDTO
    {
        public int? Id { get; set; }

        public string Code { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public double? Expense { get; set; }

        public DateTime? PaymentDate { get; set; }

        public string UserCode { get; set; }
    }
}