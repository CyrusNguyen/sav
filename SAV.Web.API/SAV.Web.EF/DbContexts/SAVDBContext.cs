namespace SAV.Web.EF.DbContexts
{
    using SAV.Web.Entities;
    using System.Data.Entity;

    /// <summary>
    /// Represents for a DbContext inherit base context.
    /// </summary>
    public partial class SAVDBContext : DbContext
    {
        /// <summary>
        /// Defines a construct for context using name or connection string.
        /// </summary>
        /// <param name="nameOrConnectionString">Defines a name or connection string in the web config.</param>
        public SAVDBContext(string nameOrConnectionString = "name=SAVDBConnection")
            : base(nameOrConnectionString)
        {
        }

        /// <summary>
        /// Defines a dbset for table logger.
        /// </summary>
        public virtual DbSet<Logger> Loggers { get; set; }

        /// <summary>
        /// Defines a dbset for table spending.
        /// </summary>
        public virtual DbSet<Spending> Spendings { get; set; }

        /// <summary>
        /// Defines a dbset for table spending history.
        /// </summary>
        public virtual DbSet<SpendingHistory> SpendingHistories { get; set; }

        /// <summary>
        /// Defines a dbset for table user.
        /// </summary>
        public virtual DbSet<User> Users { get; set; }

        /// <summary>
        /// Defines a dbset for table wallet.
        /// </summary>
        public virtual DbSet<Wallet> Wallets { get; set; }

        /// <summary>
        /// Defines a dbset for table wallet history.
        /// </summary>
        public virtual DbSet<WalletHistory> WalletHistories { get; set; }

        /// <summary>
        /// Defines a dbset for table wallet type.
        /// </summary>
        public virtual DbSet<WalletType> WalletTypes { get; set; }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized,
        /// but before the model has been locked down and used to initialize the context.
        /// The default implementation of this method does nothing, but it can be overridden
        /// in a derived class such that the model can be further configured before it is
        /// locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Logger>()
                .Property(e => e.UserCode)
                .IsUnicode(false);

            modelBuilder.Entity<Spending>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Spending>()
                .Property(e => e.UserCode)
                .IsUnicode(false);

            modelBuilder.Entity<SpendingHistory>()
                .Property(e => e.WalletCode)
                .IsUnicode(false);

            modelBuilder.Entity<SpendingHistory>()
                .Property(e => e.SpendingCode)
                .IsUnicode(false);

            modelBuilder.Entity<SpendingHistory>()
                .Property(e => e.ActionCode)
                .IsUnicode(false);

            modelBuilder.Entity<SpendingHistory>()
                .Property(e => e.UserCode)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.TokenCode)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Wallet>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Wallet>()
                .Property(e => e.UserCode)
                .IsUnicode(false);

            modelBuilder.Entity<WalletHistory>()
                .Property(e => e.WalletCode)
                .IsUnicode(false);

            modelBuilder.Entity<WalletHistory>()
                .Property(e => e.ReferenceCode)
                .IsUnicode(false);

            modelBuilder.Entity<WalletHistory>()
                .Property(e => e.WayCode)
                .IsUnicode(false);

            modelBuilder.Entity<WalletHistory>()
                .Property(e => e.UserCode)
                .IsUnicode(false);

            modelBuilder.Entity<WalletType>()
                .Property(e => e.Key)
                .IsUnicode(false);

            modelBuilder.Entity<WalletType>()
                .Property(e => e.UserCode)
                .IsUnicode(false);
        }
    }
}