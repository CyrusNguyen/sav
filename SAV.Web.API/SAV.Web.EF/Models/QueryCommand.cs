﻿using System;
using System.Data.Entity;

namespace SAV.Web.EF.Models
{
    public class QueryCommand
    {
        public string CommandText { get; set; }

        public QueryType CommandType { get; set; }

        public DbContext CommandContext { get; set; }

        public Type CommandTypeParameter { get; set; }
    }
}