﻿using System;

namespace SAV.Web.EF.Models
{
    [Flags]
    public enum QueryType
    {
        Text = 0,
        StoredProceduce = 1
    }
}