﻿using log4net;
using SAV.Web.EF.DbContexts.Genarates.Stored.Entity;
using SAV.Web.EF.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace SAV.Web.EF.Extensions
{
    /// <summary>
    /// The db context hepler.
    /// Include extensions support for db context.
    /// </summary>
    public static class DbContextExtention
    {
        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DbContextExtention));

        /// <summary>
        /// The execute for stored with sql query.
        /// </summary>
        /// <typeparam name="TStoredParameter">Represents for name of stored.</typeparam>
        /// <param name="dbContext">Represents for db context.</param>
        /// <returns>Returns a query command.</returns>
        public static QueryCommand SqlStored<TStoredParameter>(this DbContext dbContext) where TStoredParameter : IStoredParameter
        {
            try
            {
                if (dbContext == null)
                {
                    throw new ArgumentNullException(nameof(dbContext));
                }

                var storedName = typeof(TStoredParameter).Name;
                if (string.IsNullOrEmpty(storedName))
                {
                    throw new ArgumentNullException(nameof(storedName));
                }

                var cmd = new QueryCommand
                {
                    CommandText = string.Format("EXEC [{0}] ", storedName),
                    CommandType = QueryType.StoredProceduce,
                    CommandContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext)),
                    CommandTypeParameter = typeof(TStoredParameter)
                };

                return cmd;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The parameter for a stored with a tuple type.
        /// </summary>
        /// <param name="queryCommand">The db command of db context.</param>
        /// <param name="parameters">The properties represent for parameters of stored.</param>
        /// <returns>Returns a db command contain name, type command and parameters of stored.</returns>
        public static QueryCommand WithSqlParams(this QueryCommand queryCommand, params Tuple<string, object>[] parameters)
        {
            try
            {
                if (queryCommand == null)
                {
                    throw new ArgumentNullException(nameof(queryCommand));
                }

                if (parameters == null)
                {
                    throw new ArgumentNullException(nameof(parameters));
                }

                var colParams = parameters.Select(col => col).ToDictionary(col => col.Item1);
                var storeds = queryCommand.CommandTypeParameter.GetProperties();

                if (colParams.Count() == 0)
                {
                    throw new Exception("The parameter not enough properties.");
                }

                if (storeds.Count() == 0)
                {
                    throw new Exception("The stored not exists parameters.");
                }

                string paramString = "";
                int mapping = 0;
                for (int i = 0; i < storeds.Length; i++)
                {
                    if (colParams.ContainsKey(storeds[i].Name))
                    {
                        var typeProperty = colParams[storeds[i].Name];
                        var typeParameter = storeds[i];

                        mapping++;
                        var comma = ((i < storeds.Length - 1) ? ", " : " ");
                        var value = colParams[storeds[i].Name].Item2;
                        var valueFormat = string.Format(typeParameter.EqualDataType("String") ? " N'{0}' " : typeParameter.EqualDataType("DateTime") ? " '{0}' " : " {0} ", value ?? DBNull.Value);

                        paramString += string.Format(" @{0} = {1} {2} ", storeds[i].Name, valueFormat, comma);
                    }
                }

                if (mapping != storeds.Length)
                {
                    throw new Exception("The parameter mapping not equals about data type and name of Properties.");
                }

                queryCommand.CommandText = string.Format("{0} {1}", queryCommand.CommandText, paramString);
                return queryCommand;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The parameter for a stored with a tuple type.
        /// </summary>
        /// <typeparam name="TParameter">The instance with properties is parameters of stored.</typeparam>
        /// <param name="queryCommand">The db command of db context.</param>
        /// <param name="parameter">The properties represent for parameters of stored.</param>
        /// <returns>Returns a db command contain name, type command and parameters of stored.</returns>
        public static QueryCommand WithSqlParams<TParameter>(this QueryCommand queryCommand, TParameter parameter)
        {
            try
            {
                if (queryCommand == null)
                {
                    throw new ArgumentNullException(nameof(queryCommand));
                }

                if (parameter == null)
                {
                    throw new ArgumentNullException(nameof(parameter));
                }

                var colParams = parameter.GetType().GetProperties().Select(col => col).ToDictionary(col => col.Name);
                var storeds = queryCommand.CommandTypeParameter.GetProperties();

                if (colParams.Count() == 0)
                {
                    throw new Exception("The parameter not enough properties.");
                }

                if (storeds.Count() == 0)
                {
                    throw new Exception("The stored not exists parameters.");
                }

                string paramString = "";
                int mapping = 0;
                for (int i = 0; i < storeds.Length; i++)
                {
                    if (colParams.ContainsKey(storeds[i].Name))
                    {
                        var typeProperty = colParams[storeds[i].Name];
                        var typeParameter = storeds[i];
                        if (typeParameter.EqualDataType(typeProperty))
                        {
                            mapping++;
                            var comma = ((i < storeds.Length - 1) ? ", " : " ");
                            var value = typeProperty?.GetValue(parameter);
                            var valueFormat = string.Format(typeParameter.EqualDataType("String") ? " N'{0}' " : typeParameter.EqualDataType("DateTime") ? " '{0}' " : " {0} ", value ?? DBNull.Value);

                            paramString += string.Format(" @{0} = {1} {2} ", storeds[i].Name, valueFormat, comma);
                        }
                    }
                }

                if (mapping != storeds.Length)
                {
                    throw new Exception("The parameter mapping not equals about data type and name of Properties.");
                }

                queryCommand.CommandText = string.Format("{0} {1}", queryCommand.CommandText, paramString);
                return queryCommand;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The check data type of property with string data type.
        /// </summary>
        /// <param name="property">The property info is contain information of a property.</param>
        /// <param name="dataType">The data type.</param>
        /// <returns>Return true/false.</returns>
        private static bool EqualDataType(this PropertyInfo property, string dataType)
        {
            try
            {
                var typeProperty = property.PropertyType;
                var stringType = ((typeProperty.GenericTypeArguments.Length > 0) ? typeProperty.GenericTypeArguments[0].Name : typeProperty.Name);
                if (stringType.CompareTo(dataType) == 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The compare data type of properties.
        /// </summary>
        /// <param name="left">The property left.</param>
        /// <param name="right">The property right.</param>
        /// <returns>Returns true/false compare data type.</returns>
        private static bool EqualDataType(this PropertyInfo left, PropertyInfo right)
        {
            try
            {
                var genericTypes = left.PropertyType.GenericTypeArguments;
                var stringTypeProperty = ((genericTypes.Length > 0) ? genericTypes[0].Name : left.PropertyType.Name);
                var stringTypeParameter = (right.PropertyType.GenericTypeArguments.Length > 0) ? right.PropertyType.GenericTypeArguments[0].Name : right.PropertyType.Name;

                if (stringTypeProperty.CompareTo(stringTypeParameter) == 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        /// <summary>
        /// The execute stored and return a list interface object.
        /// If generic type dose not properties the same with columns of result to stored the return null also does have return result to stored.
        /// </summary>
        /// <typeparam name="TMap">Represent for result type.</typeparam>
        /// <param name="queryCommand">The db command of db context.</param>
        /// <returns>Returns records is result of a stored.</returns>
        public static TMap[] ToExecute<TMap>(this QueryCommand queryCommand) where TMap : class
        {
            try
            {
                if (queryCommand == null)
                {
                    throw new ArgumentNullException(nameof(queryCommand));
                }

                var values = queryCommand?.CommandContext?.Database?.SqlQuery<TMap>(queryCommand.CommandText)?.ToArray();
                return values;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }
    }
}