﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.Wallet
{
    /// <summary>
    ///
    /// </summary>
    public class WalletAddViewModel
    {
        /// <summary>
        /// Represent for parameter name of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Represent for parameter usercode of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [Range(0.0, Double.MaxValue)]
        public double? Balance { get; set; }

        /// <summary>
        /// Represent for parameter typecode of this stored.
        /// </summary>
        [Required]
        //[RegularExpression("([0-9]+)",ErrorMessage ="Please input number")]
        [Range(0, int.MaxValue)]
        public int? TypeCode { get; set; }
    }
}