﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.Wallet.Money
{
    /// <summary>
    ///
    /// </summary>
    public class WalletMoneyViewModel
    {
        /// <summary>
        /// Represent for parameter walletcode of this stored.
        /// </summary>
        [Required]
        [StringLength(70)]
        public string WalletCode { get; set; }

        /// <summary>
        /// Represent for parameter Amount of this stored.
        /// </summary>
        [Required]
        [Range(0.0, Double.MaxValue)]
        public double? Amount { get; set; }

        /// <summary>
        /// Represent for parameter description of this stored.
        /// </summary>
        [StringLength(1000)]
        public string Description { get; set; }

        /// <summary>
        /// Represent for parameter usercode of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}