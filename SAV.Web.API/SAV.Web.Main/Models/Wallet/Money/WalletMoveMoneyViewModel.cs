﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.Wallet.Money
{
    /// <summary>
    ///
    /// </summary>
    public class WalletMoveMoneyViewModel
    {
        [Required]
        [StringLength(70)]
        public string WalletCode { get; set; }

        /// <summary>
        /// Represent for parameter referencecode of this stored.
        /// </summary>
        [Required]
        [StringLength(500)]
        public string ReferenceCode { get; set; }

        /// <summary>
        /// Represent for parameter description of this stored.
        /// </summary>
        [StringLength(1000)]
        public string Description { get; set; }

        /// <summary>
        /// Represent for parameter expense of this stored.
        /// </summary>
        [Required]
        [Range(0.0, Double.MaxValue)]
        public double? Expense { get; set; }

        /// <summary>
        /// Represent for parameter usercode of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}