﻿using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.Wallet
{
    /// <summary>
    ///
    /// </summary>
    public class WalletUpdateViewModel
    {
        /// <summary>
        /// Represent for parameter name of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Represent for parameter usercode of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }

        /// <summary>
        /// Represent for parameter typecode of this stored.
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int? TypeCode { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(70)]
        public string WalletCode { get; set; }
    }
}