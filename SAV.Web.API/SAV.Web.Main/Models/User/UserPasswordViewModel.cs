﻿using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.User
{
    /// <summary>
    ///
    /// </summary>
    public class UserPasswordViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Key]
        [StringLength(100)]
        public string Code { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MinLength(6)]
        [StringLength(500)]
        public string Username { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MinLength(8)]
        [StringLength(500)]
        public string PasswordOld { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MinLength(8)]
        [StringLength(500)]
        public string PasswordNew { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(500)]
        public string TokenCode { get; set; }
    }
}