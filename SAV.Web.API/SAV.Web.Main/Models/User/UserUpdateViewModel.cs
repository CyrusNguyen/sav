﻿using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.User
{
    /// <summary>
    ///
    /// </summary>
    public class UserUpdateViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Key]
        [StringLength(100)]
        public string Code { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MinLength(6)]
        [StringLength(500)]
        public string Username { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MinLength(8)]
        [StringLength(500)]
        public string Password { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(500)]
        public string TokenCode { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(100)]
        public string FullName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MaxLength(11)]
        [MinLength(10)]
        [StringLength(20)]
        [RegularExpression("([0-9]+)")]
        public string Phone { get; set; }
    }
}