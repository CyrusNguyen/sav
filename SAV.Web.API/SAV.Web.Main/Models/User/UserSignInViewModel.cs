﻿using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.User
{
    /// <summary>
    ///
    /// </summary>
    public class UserSignInViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Required]
        [MinLength(6)]
        [StringLength(500)]
        public string Username { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MinLength(8)]
        [StringLength(500)]
        public string Password { get; set; }
    }
}