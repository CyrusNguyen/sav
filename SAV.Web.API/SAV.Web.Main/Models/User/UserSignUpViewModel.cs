﻿using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.User
{
    /// <summary>
    ///
    /// </summary>
    public class UserSignUpViewModel : UserSignInViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(100)]
        public string FullName { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [MaxLength(11)]
        [MinLength(10)]
        [StringLength(20)]
        [RegularExpression("([0-9]+)")]
        public string Phone { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Role { get; set; }
    }
}