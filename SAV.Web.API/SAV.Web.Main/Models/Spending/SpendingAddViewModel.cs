﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.Spending
{
    /// <summary>
    /// Represents for parameter of action add.
    /// </summary>
    public class SpendingAddViewModel
    {
        /// <summary>
        /// Represent for parameter title of this stored.
        /// </summary>
        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        /// <summary>
        /// Represent for parameter description of this stored.
        /// </summary>
        [StringLength(1000)]
        public string Description { get; set; }

        /// <summary>
        /// Represent for parameter expense of this stored.
        /// </summary>
        [Required]
        [Range(0.0, Double.MaxValue)]
        public double? Expense { get; set; }

        /// <summary>
        /// Represent for parameter paymentdate of this stored.
        /// </summary>
        [Required]
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// Represent for parameter walletcode of this stored.
        /// </summary>
        [Required]
        [StringLength(140)]
        public string WalletCode { get; set; }

        /// <summary>
        /// Represent for parameter usercode of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}