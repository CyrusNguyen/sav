﻿using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.Spending
{
    /// <summary>
    ///
    /// </summary>
    public class SpendingDeleteViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(70)]
        public string Code { get; set; }

        /// <summary>
        /// Represent for parameter usercode of this stored.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string UserCode { get; set; }
    }
}