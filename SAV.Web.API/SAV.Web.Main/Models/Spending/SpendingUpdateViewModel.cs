﻿using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.Spending
{
    /// <summary>
    ///
    /// </summary>
    public class SpendingUpdateViewModel : SpendingAddViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(70)]
        public string Code { get; set; }
    }
}