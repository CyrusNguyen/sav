using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.WalletType
{
    /// <summary>
    ///
    /// </summary>
    public partial class WalletTypeDeleteViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int? Id { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(400)]
        public string UserCode { get; set; }
    }
}