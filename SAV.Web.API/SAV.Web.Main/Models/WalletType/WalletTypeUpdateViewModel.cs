using System.ComponentModel.DataAnnotations;

namespace SAV.Web.Main.Models.WalletType
{
    /// <summary>
    ///
    /// </summary>
    public partial class WalletTypeUpdateViewModel
    {
        /// <summary>
        ///
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int? Id { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(70)]
        [RegularExpression(@"^\S*$", ErrorMessage = "In the value does not contain any space.")]
        public string Key { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        /// <summary>
        ///
        /// </summary>
        [Required]
        [StringLength(400)]
        public string UserCode { get; set; }
    }
}