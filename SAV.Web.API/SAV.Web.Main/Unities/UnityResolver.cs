﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;
using Unity;

namespace SAV.Web.Main.Utilites
{
    /// <summary>
    /// Represents for a dependency resolver in web config.
    /// Using auto create new a instance when detect a construct or property using IDependencyResolver interface inherit.
    /// </summary>
    public class UnityResolver : IDependencyResolver
    {
        /// <summary>
        /// A container used to store your objects initialized.
        /// </summary>
        protected IUnityContainer container;

        /// <summary>
        /// Constructs a new unity resolver using a container interface made dependency inject.
        /// </summary>
        /// <param name="container"></param>
        public UnityResolver(IUnityContainer container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        /// <summary>
        /// Get a object exist in container.
        /// </summary>
        /// <param name="serviceType">Represent the desired injection case object.</param>
        /// <returns>Returns a object is type argument.</returns>
        public object GetService(Type serviceType)
        {
            try
            {
                return container.Resolve(serviceType);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get objects exist in container.
        /// </summary>
        /// <param name="serviceType">Represent the desired injection case object.</param>
        /// <returns>Returns objects is type argument.</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }
            catch
            {
                return Enumerable.Empty<object>();
            }
        }

        /// <summary>
        /// The start a dependency scope.
        /// </summary>
        /// <returns>Returns denpendency scope.</returns>
        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            return new UnityResolver(child);
        }

        /// <summary>
        ///  Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources of container.
        /// </summary>
        public void Dispose()
        {
            container.Dispose();
        }
    }
}