﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.Commons.Mappings;
using SAV.Web.DTO.Wallet;
using SAV.Web.Main.ApiControllers.Base;
using SAV.Web.Main.Models.Wallet;
using SAV.Web.Main.Models.Wallet.Money;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace SAV.Web.Main.ApiControllers
{
    /// <summary>
    /// Represents for a wallet controller inherit api controller.
    /// </summary>
    public class WalletController : BaseController<IWalletBUS>
    {
        /// <summary>
        /// A constructure using a injection constructor for wallet controller.
        /// </summary>
        /// <param name="baseBUS">Represents for interface of wallet bus layer.</param>
        /// <param name="log">Represent for a log model.</param>
        [InjectionConstructor]
        public WalletController(IWalletBUS baseBUS, ILog log) : base(baseBUS, log)
        {
        }

        [HttpGet]
        public HttpResponseMessage GetWallet(string username)
        {
            try
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var value = _baseBUS.GetWallet(username);
                    return SuccessList(value);
                }

                return ErrorString("The user name is required.");
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Add([FromBody] WalletAddViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<WalletParameterDTO>();
                    var value = _baseBUS.Add(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Update([FromBody] WalletUpdateViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<WalletParameterDTO>();
                    var value = _baseBUS.Update(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletDeleteViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Delete([FromBody] WalletDeleteViewModel walletDeleteViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletDeleteViewModel.Mapping<WalletParameterDTO>();
                    var value = _baseBUS.Delete(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletMoneyViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage AddMoney([FromBody] WalletMoneyViewModel walletMoneyViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletMoneyViewModel.Mapping<WalletParameterDTO>();
                    var value = _baseBUS.AddMoney(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletMoneyViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SubMoney([FromBody] WalletMoneyViewModel walletMoneyViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletMoneyViewModel.Mapping<WalletParameterDTO>();
                    var value = _baseBUS.SubMoney(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletMoneyViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage MoveMoney([FromBody] WalletMoveMoneyViewModel walletMoneyViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletMoneyViewModel.Mapping<WalletParameterDTO>();
                    var value = _baseBUS.MoveMoney(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }
    }
}