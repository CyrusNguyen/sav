﻿using log4net;
using SAV.Web.Commons.Jsons;
using System;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SAV.Web.Main.ApiControllers.Base
{
    /// <summary>
    /// Represents the base controller for the api controller.
    /// Use the same management parts in the controller.
    /// </summary>
    public class BaseController<TBaseBUS> : ApiController
    {
        /// <summary>
        /// Represents for a business layer.
        /// </summary>
        protected readonly TBaseBUS _baseBUS;

        /// <summary>
        /// Write log for web layer.
        /// </summary>
        protected readonly ILog _log;

        /// <summary>
        /// Constructs a base controller.
        /// Using dependency injection with business interface layer.
        /// </summary>
        /// <param name="baseBUS">Represents for business layer injection.</param>
        public BaseController(TBaseBUS baseBUS, ILog log)
        {
            _log = log ?? throw new ArgumentNullException(nameof(log));
            _baseBUS = baseBUS;
        }

        /// <summary>
        /// Return a message success to client.
        /// </summary>
        /// <typeparam name="T">Represents for data tye of value.</typeparam>
        /// <param name="data">The data sent to client.</param>
        /// <returns>Return a http response message.</returns>
        protected HttpResponseMessage SuccessData<TEntity>(TEntity data) where TEntity : class
        {
            return Request.CreateResponse(HttpStatusCode.OK, data?.ToJObject(), Configuration.Formatters.JsonFormatter);
        }

        protected HttpResponseMessage SuccessList<TList>(TList data) where TList : IEnumerable
        {
            return Request.CreateResponse(HttpStatusCode.OK, data?.ToJArray(), Configuration.Formatters.JsonFormatter);
        }

        protected HttpResponseMessage SuccessString(string message)
        {
            return Request.CreateResponse(HttpStatusCode.OK, message, Configuration.Formatters.JsonFormatter);
        }

        /// <summary>
        /// Return a message error to client.
        /// </summary>
        /// <param name="ex">The exception sent to client.</param>
        /// <returns>Return a http response message.</returns>
        protected HttpResponseMessage ErrorException(Exception ex)
        {
            _log.Error(ex);
            return Request.CreateResponse(HttpStatusCode.InternalServerError, ex?.ToJObject(), Configuration.Formatters.JsonFormatter);
        }

        protected HttpResponseMessage ErrorString(string message)
        {
            _log.Error(message);
            return Request.CreateResponse(HttpStatusCode.InternalServerError, message, Configuration.Formatters.JsonFormatter);
        }

        /// <summary>
        /// Return a message error to client.
        /// </summary>
        /// <param name="errors">The exception sent to client.</param>
        /// <returns>Return a http response message.</returns>
        protected HttpResponseMessage ThrowValition(object errors)
        {
            _log.Error(errors);
            return Request.CreateResponse(HttpStatusCode.InternalServerError, errors?.ToJObject(), Configuration.Formatters.JsonFormatter);
        }
    }
}