﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.Commons.Mappings;
using SAV.Web.DTO.User;
using SAV.Web.Main.ApiControllers.Base;
using SAV.Web.Main.Models.User;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Unity;

namespace SAV.Web.Main.ApiControllers
{
    /// <summary>
    /// Represents for a user controller inherit api controller.
    /// </summary>
    public class UserController : BaseController<IUserBUS>
    {
        /// <summary>
        /// A constructure using a injection constructor for user controller.
        /// </summary>
        /// <param name="baseBUS">Represents for interface of user bus layer.</param>
        /// <param name="log">Represent for a log model.</param>
        [InjectionConstructor]
        public UserController(IUserBUS baseBUS, ILog log) : base(baseBUS, log)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userAddViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SignUp([FromBody] UserSignUpViewModel userAddViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = userAddViewModel.Mapping<UserParameterDTO>();
                    var value = _baseBUS.SignUp(user);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="loginViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SignIn([FromBody] UserSignInViewModel loginViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var loginUser = loginViewModel.Mapping<UserParameterDTO>();
                    var value = _baseBUS.SignIn(loginUser);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userUpdateViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Update([FromBody] UserUpdateViewModel userUpdateViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var updateUser = userUpdateViewModel.Mapping<UserParameterDTO>();
                    var value = _baseBUS.Update(updateUser);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userUpdateViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdatePassword([FromBody] UserPasswordViewModel userUpdateViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var updateUser = userUpdateViewModel.Mapping<UserParameterDTO>();
                    var value = _baseBUS.UpdatePassword(updateUser);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }
    }
}