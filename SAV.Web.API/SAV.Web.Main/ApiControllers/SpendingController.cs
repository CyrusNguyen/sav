﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.Commons.Mappings;
using SAV.Web.DTO.Spending;
using SAV.Web.Main.ApiControllers.Base;
using SAV.Web.Main.Models.Spending;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace SAV.Web.Main.ApiControllers
{
    /// <summary>
    /// Represents for a spending controller inherit api controller.
    /// </summary>
    public class SpendingController : BaseController<ISpendingBUS>
    {
        /// <summary>
        /// A constructure using a injection constructor for spending controller.
        /// </summary>
        /// <param name="baseBUS">Represents for interface of spending bus layer.</param>
        /// <param name="log">Represent for a log model.</param>
        [InjectionConstructor]
        public SpendingController(ISpendingBUS baseBUS, ILog log) : base(baseBUS, log)
        {
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return SuccessString("You can make it!");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage InsertDaily([FromBody] SpendingAddViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<SpendingParameterDTO>();
                    var value = _baseBUS.InsertDaily(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
#pragma warning disable IDE0067 // Dispose objects before losing scope
                return ErrorException(ex);
#pragma warning restore IDE0067 // Dispose objects before losing scope
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage UpdateDaily([FromBody] SpendingUpdateViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<SpendingParameterDTO>();
                    var value = _baseBUS.UpdateDaily(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
#pragma warning disable IDE0067 // Dispose objects before losing scope
                return ErrorException(ex);
#pragma warning restore IDE0067 // Dispose objects before losing scope
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage DeleteDaily([FromBody] SpendingDeleteViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<SpendingParameterDTO>();
                    var value = _baseBUS.DeleteDaily(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
#pragma warning disable IDE0067 // Dispose objects before losing scope
                return ErrorException(ex);
#pragma warning restore IDE0067 // Dispose objects before losing scope
            }
        }
    }
}