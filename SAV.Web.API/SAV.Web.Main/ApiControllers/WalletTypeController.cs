﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.Commons.Mappings;
using SAV.Web.DTO.WalletType;
using SAV.Web.Main.ApiControllers.Base;
using SAV.Web.Main.Models.WalletType;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace SAV.Web.Main.ApiControllers
{
    /// <summary>
    /// Represents for a wallet type controller inherit api controller.
    /// </summary>
    public class WalletTypeController : BaseController<IWalletTypeBUS>
    {
        /// <summary>
        /// A constructure using a injection constructor for wallet type controller.
        /// </summary>
        /// <param name="baseBUS">Represents for interface of wallet type bus layer.</param>
        /// <param name="log">Represent for a log model.</param>
        [InjectionConstructor]
        public WalletTypeController(IWalletTypeBUS baseBUS, ILog log) : base(baseBUS, log)
        {
        }

        [HttpGet]
        public HttpResponseMessage GetWalletType(string username)
        {
            try
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var value = _baseBUS.GetWalletType(username);
                    return SuccessList(value);
                }

                return ErrorString("The user name is required.");
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Add([FromBody] WalletTypeAddViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<WalletTypeParameterDTO>();
                    var value = _baseBUS.Add(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Update([FromBody] WalletTypeUpdateViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<WalletTypeParameterDTO>();
                    var value = _baseBUS.Update(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletItemViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Delete([FromBody] WalletTypeDeleteViewModel walletItemViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var paramers = walletItemViewModel.Mapping<WalletTypeParameterDTO>();
                    var value = _baseBUS.Delete(paramers);
                    return SuccessData(value);
                }

                return ThrowValition(ModelState.Values.Select(v => v.Errors.Select(e => e.ErrorMessage)));
            }
            catch (Exception ex)
            {
                return ErrorException(ex);
            }
        }
    }
}