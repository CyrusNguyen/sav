﻿using log4net;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SAV.Web.Main
{
    /// <summary>
    /// Represent for life cycle of web application.
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The method called the first when application run.
        /// Using configugration when application start.
        /// </summary>
        protected void Application_Start()
        {
            // configuration for log4net.
            GlobalContext.Properties["LogPath"] = string.Format(@"logs\{0}", DateTime.Now.ToString("yyyyMMdd"));
            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}