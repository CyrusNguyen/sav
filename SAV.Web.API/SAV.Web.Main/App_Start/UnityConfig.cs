﻿using log4net;
using SAV.Web.BUS.Interfaces;
using SAV.Web.Commons.Unities;
using SAV.Web.Main.ApiControllers;
using System;
using Unity;

namespace SAV.Web.Main
{
    /// <summary>
    /// Represents for unity config of web layer.
    /// </summary>
    public class UnityConfig
    {
        /// <summary>
        /// A container used to store your objects initialized.
        /// </summary>
        private static IUnityContainer _container;

        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(IUnityContainer));

        /// <summary>
        /// A container used to store your objects initialized.
        /// Used to return a container.
        /// </summary>
        public static IUnityContainer Containers
        {
            get
            {
                if (_container == null)
                {
                    _container = new UnityContainer();
                }

                return GetContainers();
            }
        }

        /// <summary>
        /// Register type, singleton, instance, and factory for container.
        /// </summary>
        /// <returns>Returns a container.</returns>
        private static IUnityContainer GetContainers()
        {
            try
            {
                // Config container for BUS layer.
                BUS.UnityConfig.SetContainers(ref _container);

                // Config container for WEB layer.
                _container.RegisterInstance<SpendingController, ISpendingBUS>("SpendingBUS", LogManager.GetLogger(typeof(SpendingController)));
                _container.RegisterInstance<WalletController, IWalletBUS>("WalletBUS", LogManager.GetLogger(typeof(WalletController)));
                _container.RegisterInstance<WalletTypeController, IWalletTypeBUS>("WalletTypeBUS", LogManager.GetLogger(typeof(WalletTypeController)));
                _container.RegisterInstance<UserController, IUserBUS>("UserBUS", LogManager.GetLogger(typeof(UserController)));

                return _container;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return default;
            }
        }
    }
}