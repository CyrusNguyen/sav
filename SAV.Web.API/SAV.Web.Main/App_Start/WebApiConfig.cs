﻿using SAV.Web.Main.Utilites;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SAV.Web.Main
{
    /// <summary>
    /// Represents for web config.
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Register web config as map http attribute routes, dependency injection, configuration and services.
        /// </summary>
        /// <param name="config">Represents a http configuration.</param>
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("http://localhost:4200", "*", "*");
            config.EnableCors(cors);
            // Web API configuration and services.

            // Web API routes.
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Web API dependency injection resolver.
            config.DependencyResolver = new UnityResolver(Main.UnityConfig.Containers);
        }
    }
}