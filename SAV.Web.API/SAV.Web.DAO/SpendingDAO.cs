﻿using log4net;
using SAV.Web.Commons.DataAnnotations;
using SAV.Web.Commons.Mappings;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DbRepositories.Interfaces;
using SAV.Web.DTO.Commons;
using SAV.Web.DTO.Spending;
using SAV.Web.EF.DbContexts.Genarates.Stored;
using SAV.Web.EF.Extensions;
using System;
using System.Data.Entity.Validation;
using System.Linq;

namespace SAV.Web.DAO
{
    /// <summary>
    ///
    /// </summary>
    public class SpendingDAO : BaseDAO, ISpendingDAO
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="dbRepository"></param>
        /// <param name="log"></param>
        public SpendingDAO(IDbRepository dbRepository, ILog log) : base(dbRepository, log) { }

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        public ResultDTO DeleteDaily(SpendingParameterDTO spendingParameterDTO)
        {
            try
            {
                var parameter = spendingParameterDTO.Mapping<sp_sav_spending_DeleteDaily>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_spending_DeleteDaily>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        public SpendingResultDTO InsertDaily(SpendingParameterDTO spendingParameterDTO)
        {
            try
            {
                var parameter = spendingParameterDTO.Mapping<sp_sav_spending_InsertDaily>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_spending_InsertDaily>().WithSqlParams(parameter).ToExecute<SpendingResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        public ResultDTO UpdateDaily(SpendingParameterDTO spendingParameterDTO)
        {
            try
            {
                var parameter = spendingParameterDTO.Mapping<sp_sav_spending_UpdateDaily>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_spending_UpdateDaily>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}