﻿using log4net;
using SAV.Web.Commons.DataAnnotations;
using SAV.Web.Commons.Mappings;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DbRepositories.Interfaces;
using SAV.Web.DTO.Commons;
using SAV.Web.DTO.Wallet;
using SAV.Web.EF.DbContexts.Genarates.Stored;
using SAV.Web.EF.Extensions;
using SAV.Web.Entities;
using System;
using System.Data.Entity.Validation;
using System.Linq;

namespace SAV.Web.DAO
{
    /// <summary>
    ///
    /// </summary>
    public class WalletDAO : BaseDAO, IWalletDAO
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="dbRepository"></param>
        /// <param name="log"></param>
        public WalletDAO(IDbRepository dbRepository, ILog log) : base(dbRepository, log)
        {
        }

        #region Get

        public WalletCommonDTO[] GetWallet(string username)
        {
            if (!string.IsNullOrEmpty(username))
            {
                using (var db = _dbRepository.GetContext())
                {
                    var wallettypes = db.Set<WalletType>().ToArray();
                    var wallet = db.Set<Wallet>().ToArray()
                                    .Join(
                                    wallettypes,
                                    w => w.TypeCode
                                    , wt => wt.Id,
                                    (w, wt) => new WalletCommonDTO
                                    {
                                        IsDeleted = (bool)w.IsDeleted,
                                        UserCode = w.UserCode,
                                        Code = w.Code,
                                        Name = w.Name,
                                        Balance = w.Balance,
                                        //w.IsDefault,
                                        TypeId = wt.Id,
                                        TypeName = wt.Name,
                                        TypeKey = wt.Key
                                    })
                                    .Where(wt => wt.IsDeleted == false && wt.UserCode.Equals(username)).ToArray();

                    return wallet.MappingList<WalletCommonDTO[]>();
                }
            }

            throw new Exception("The username is required.");
        }

        #endregion Get

        public WalletResultDTO Add(WalletParameterDTO walletDTO)
        {
            try
            {
                var parameter = walletDTO.Mapping<sp_sav_wallet_Add>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallet_Add>().WithSqlParams(parameter).ToExecute<WalletResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultDTO AddMoney(WalletParameterDTO walletDTO)
        {
            try
            {
                var parameter = walletDTO.Mapping<sp_sav_wallet_AddMoney>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallet_AddMoney>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultDTO Delete(WalletParameterDTO walletDTO)
        {
            try
            {
                var parameter = walletDTO.Mapping<sp_sav_wallet_Delete>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallet_Delete>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultDTO MoveMoney(WalletParameterDTO walletDTO)
        {
            try
            {
                var parameter = walletDTO.Mapping<sp_sav_wallet_MoveMoney>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallet_MoveMoney>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultDTO SubMoney(WalletParameterDTO walletDTO)
        {
            try
            {
                var parameter = walletDTO.Mapping<sp_sav_wallet_SubMoney>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallet_SubMoney>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResultDTO Update(WalletParameterDTO walletDTO)
        {
            try
            {
                var parameter = walletDTO.Mapping<sp_sav_wallet_Update>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallet_Update>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}