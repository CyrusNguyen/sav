﻿using log4net;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DbRepositories.Interfaces;
using System;

namespace SAV.Web.DAO
{
    /// <summary>
    ///
    /// </summary>
    public class BaseDAO : IBaseDAO
    {
        /// <summary>
        ///
        /// </summary>
        protected readonly IDbRepository _dbRepository;

        /// <summary>
        ///
        /// </summary>
        protected readonly ILog _log;

        /// <summary>
        ///
        /// </summary>
        /// <param name="dbRepository"></param>
        /// <param name="log"></param>
        public BaseDAO(IDbRepository dbRepository, ILog log)
        {
            _log = log ?? throw new ArgumentNullException(nameof(log));
            _dbRepository = dbRepository ?? throw new ArgumentNullException(nameof(dbRepository));
        }
    }
}