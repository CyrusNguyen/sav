﻿using log4net;
using SAV.Web.Commons.DataAnnotations;
using SAV.Web.Commons.Mappings;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DbRepositories.Interfaces;
using SAV.Web.DTO.Commons;
using SAV.Web.DTO.WalletType;
using SAV.Web.EF.DbContexts.Genarates.Stored;
using SAV.Web.EF.Extensions;
using SAV.Web.Entities;
using System;
using System.Data.Entity.Validation;
using System.Linq;

namespace SAV.Web.DAO
{
    /// <summary>
    ///
    /// </summary>
    public class WalletTypeDAO : BaseDAO, IWalletTypeDAO
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="dbRepository"></param>
        /// <param name="log"></param>
        public WalletTypeDAO(IDbRepository dbRepository, ILog log) : base(dbRepository, log)
        {
        }

        #region Get

        public WalletTypeResultDTO[] GetWalletType(string username)
        {
            try
            {
                using (var db = _dbRepository.GetContext())
                {
                    if (!string.IsNullOrEmpty(username))
                    {
                        var wallet = db.Set<WalletType>().Where(wt => wt.IsDeleted == false && wt.UserCode.Equals(username)).ToArray();
                        return wallet.MappingList<WalletTypeResultDTO[]>();
                    }
                }
                throw new Exception("The username is required.");
            }
            catch (DbEntityValidationException ex)
            {
                _log.Error(ex);
                throw;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        #endregion Get

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        public WalletTypeResultDTO Add(WalletTypeParameterDTO walletTypeDTO)
        {
            try
            {
                var parameter = walletTypeDTO.Mapping<sp_sav_wallettype_Add>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallettype_Add>().WithSqlParams(parameter).ToExecute<WalletTypeResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        public ResultDTO Delete(WalletTypeParameterDTO walletTypeDTO)
        {
            try
            {
                var parameter = walletTypeDTO.Mapping<sp_sav_wallettype_Delete>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallettype_Delete>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        public ResultDTO Update(WalletTypeParameterDTO walletTypeDTO)
        {
            try
            {
                var parameter = walletTypeDTO.Mapping<sp_sav_wallettype_Update>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_wallettype_Update>().WithSqlParams(parameter).ToExecute<ResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}