﻿using log4net;
using SAV.Web.Commons.Unities;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DbRepositories.Interfaces;
using Unity;

namespace SAV.Web.DAO
{
    /// <summary>
    /// Represents for unity config of DAO layer.
    /// </summary>
    public class UnityConfig
    {
        /// <summary>
        /// The set unity container with ref type.
        /// </summary>
        /// <param name="container">Represent for unity container root to web layer.</param>
        public static void SetContainers(ref IUnityContainer container)
        {
            // Config container for DbReposotory layer.
            DbRepositories.UnityConfig.SetContainers(ref container);

            // Config container for DAO layer.
            container.RegisterConstructor<ISpendingDAO, SpendingDAO, IDbRepository>("SpendingDAO", "SAVDBContext", LogManager.GetLogger(typeof(SpendingDAO)));
            container.RegisterConstructor<IWalletDAO, WalletDAO, IDbRepository>("WalletDAO", "SAVDBContext", LogManager.GetLogger(typeof(WalletDAO)));
            container.RegisterConstructor<IWalletTypeDAO, WalletTypeDAO, IDbRepository>("WalletTypeDAO", "SAVDBContext", LogManager.GetLogger(typeof(WalletTypeDAO)));
            container.RegisterConstructor<IUserDAO, UserDAO, IDbRepository>("UserDAO", "SAVDBContext", LogManager.GetLogger(typeof(UserDAO)));
        }
    }
}