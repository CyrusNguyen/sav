﻿using log4net;
using SAV.Web.Commons.DataAnnotations;
using SAV.Web.Commons.Mappings;
using SAV.Web.DAO.Interfaces;
using SAV.Web.DbRepositories.Interfaces;
using SAV.Web.DTO.User;
using SAV.Web.EF.DbContexts.Genarates.Stored;
using SAV.Web.EF.Extensions;
using System;
using System.Data.Entity.Validation;
using System.Linq;

namespace SAV.Web.DAO
{
    /// <summary>
    ///
    /// </summary>
    public class UserDAO : BaseDAO, IUserDAO
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="dbRepository"></param>
        /// <param name="log"></param>
        public UserDAO(IDbRepository dbRepository, ILog log) : base(dbRepository, log)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserResultDTO SignUp(UserParameterDTO userDTO)
        {
            try
            {
                var parameter = userDTO.Mapping<sp_sav_user_SignUp>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_user_SignUp>().WithSqlParams(parameter).ToExecute<UserResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserResultDTO Update(UserParameterDTO userDTO)
        {
            try
            {
                var parameter = userDTO.Mapping<sp_sav_user_Update>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_user_Update>().WithSqlParams(parameter).ToExecute<UserResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserResultDTO UpdatePassword(UserParameterDTO userDTO)
        {
            try
            {
                var parameter = userDTO.Mapping<sp_sav_user_UpdatePassword>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_user_UpdatePassword>().WithSqlParams(parameter).ToExecute<UserResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userLoginDTO"></param>
        /// <returns></returns>
        public UserResultDTO SignIn(UserParameterDTO userDTO)
        {
            try
            {
                var parameter = userDTO.Mapping<sp_sav_user_SignIn>();
                if (parameter.ModelState().IsValid)
                {
                    return _dbRepository.SqlStored<sp_sav_user_SignIn>().WithSqlParams(parameter).ToExecute<UserResultDTO>()?.FirstOrDefault();
                }

                throw new Exception(parameter.ModelState().Errors.FirstOrDefault().ErrorMessage);
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}