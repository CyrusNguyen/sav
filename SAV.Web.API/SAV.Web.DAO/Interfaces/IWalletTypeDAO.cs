﻿using SAV.Web.DTO.Commons;
using SAV.Web.DTO.WalletType;

namespace SAV.Web.DAO.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IWalletTypeDAO : IBaseDAO
    {
        WalletTypeResultDTO[] GetWalletType(string username);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        WalletTypeResultDTO Add(WalletTypeParameterDTO walletTypeDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        ResultDTO Update(WalletTypeParameterDTO walletTypeDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="walletTypeDTO"></param>
        /// <returns></returns>
        ResultDTO Delete(WalletTypeParameterDTO walletTypeDTO);
    }
}