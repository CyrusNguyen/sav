﻿using SAV.Web.DTO.User;

namespace SAV.Web.DAO.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IUserDAO : IBaseDAO
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        UserResultDTO SignUp(UserParameterDTO userDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        UserResultDTO SignIn(UserParameterDTO userDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        UserResultDTO Update(UserParameterDTO userDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        UserResultDTO UpdatePassword(UserParameterDTO userDTO);
    }
}