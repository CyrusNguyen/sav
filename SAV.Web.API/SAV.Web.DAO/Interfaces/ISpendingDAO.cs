﻿using SAV.Web.DTO.Commons;
using SAV.Web.DTO.Spending;

namespace SAV.Web.DAO.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface ISpendingDAO : IBaseDAO
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        SpendingResultDTO InsertDaily(SpendingParameterDTO spendingParameterDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        ResultDTO UpdateDaily(SpendingParameterDTO spendingParameterDTO);

        /// <summary>
        ///
        /// </summary>
        /// <param name="spendingParameterDTO"></param>
        /// <returns></returns>
        ResultDTO DeleteDaily(SpendingParameterDTO spendingParameterDTO);
    }
}