﻿using SAV.Web.EF.DbContexts;
using SAV.Web.EF.DbContexts.Genarates.Stored.Entity;
using SAV.Web.EF.Models;
using System.Data.Entity;

namespace SAV.Web.DbRepositories.Interfaces
{
    /// <summary>
    ///
    /// </summary>
    public interface IDbRepository
    {
        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        DbSet<TEntity> Entity<TEntity>(SAVDBContext context) where TEntity : class;

        SAVDBContext GetContext();

        /// <summary>
        ///
        /// </summary>
        /// <param name="storedName"></param>
        /// <returns></returns>
        QueryCommand SqlStored<TStoredParameter>() where TStoredParameter : IStoredParameter;
    }
}