﻿using log4net;
using SAV.Web.DbRepositories.Interfaces;
using SAV.Web.EF.DbContexts;
using SAV.Web.EF.DbContexts.Genarates.Stored.Entity;
using SAV.Web.EF.Extensions;
using SAV.Web.EF.Models;
using System.Data.Entity;

namespace SAV.Web.DbRepositories
{
    /// <summary>
    ///
    /// </summary>
    public partial class DbRepository : IDbRepository
    {
        /// <summary>
        ///
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(DbRepository));

        public DbRepository()
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public DbSet<TEntity> Entity<TEntity>(SAVDBContext context) where TEntity : class
        {
            return context.Set<TEntity>();
        }

        public SAVDBContext GetContext()
        {
            return new SAVDBContext();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="storedName"></param>
        /// <returns></returns>
        public QueryCommand SqlStored<TStoredParameter>() where TStoredParameter : IStoredParameter
        {
            return new SAVDBContext().SqlStored<TStoredParameter>();
        }
    }
}