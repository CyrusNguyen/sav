﻿using log4net;
using SAV.Web.Commons.Unities;
using SAV.Web.DbRepositories.Interfaces;
using Unity;

namespace SAV.Web.DbRepositories
{
    /// <summary>
    /// Represents for unity config of db layer.
    /// </summary>
    public class UnityConfig
    {
        /// <summary>
        ///
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(UnityConfig));

        /// <summary>
        /// The set unity container with ref type.
        /// </summary>
        /// <param name="container">Represent for unity container root to web layer.</param>
        public static void SetContainers(ref IUnityContainer container)
        {
            // Config container for DbReposotory layer.
            container.RegisterConstructor<IDbRepository, DbRepository>("SAVDBContext");
        }
    }
}