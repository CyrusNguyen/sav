import { RouterModule, Routes } from '@angular/router';
import { SpendingComponent } from './component/spending.component';

const routes: Routes = [
  {
    path: '',
    component: SpendingComponent
  },
];

export const SpendingRoutingModule = RouterModule.forChild(routes);
