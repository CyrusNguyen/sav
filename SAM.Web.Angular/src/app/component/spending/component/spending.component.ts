import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ISpendingModel } from 'src/app/models/ISpendingModel';
import { IColumn } from 'src/app/models/IColumn';
import { SpendingService } from 'src/app/services/spending.service';
import { MatSort, MatDialog } from '@angular/material';
import { ChangeSpendingComponent } from './change-spending/change-spending.component';
import { LoadingService } from 'src/app/services/loading.service';
import { MessageService } from 'src/app/services/message.service';
import { ValidationService } from 'src/app/services/commons/valid.service';
import { HelperService } from 'src/app/services/commons/helper.service';

@Component({
  selector: 'app-spending',
  templateUrl: './spending.component.html',
  styleUrls: ['./spending.component.scss']
})
export class SpendingComponent implements OnInit {

  //#region Variables
  private dataSource = new MatTableDataSource<ISpendingModel>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  private columnsToDisplay: IColumn[] = [
    { Code: 'Code', Format: 'Code' },
    { Code: 'Title', Format: 'Title' },
    { Code: 'PaymentDate', Format: 'Payment for Date' },
    { Code: 'Expense', Format: 'Amount' },
    { Code: 'WalletCode', Format: 'Wallet' },
    { Code: 'Description', Format: 'Description' }
  ];
  private displayedColumns = this.columnsToDisplay.map((column: IColumn) => column.Code);

  //#endregion

  //#region Constructors
  constructor(
    private _helperService: HelperService,
    private _spendingService: SpendingService,
    private _dialog: MatDialog,
    private _loadingService: LoadingService,
    private _messageService: MessageService) {
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.showSpendingToDay();
  }
  //#endregion

  //#region Methods
  formatData(column: string, value: ISpendingModel) {
    if (column === 'PaymentDate') {
      return this._helperService.formatDate(value[column]);
    } else
      if (column === 'Expense') {
        return this._helperService.formatCurrency(value[column]);
      } else if (column === 'Description') {
        return this._helperService.formatStringLimitLenght(value[column]);
      } else if (column === 'Title') {
        return this._helperService.formatStringLimitLenght(value[column]);
      }

    return value[column];
  }

  fillterForSpending(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showDialog(element: ISpendingModel = null): void {
    const dialogRef = this._dialog.open(ChangeSpendingComponent, {
      data: element
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  showSpendingToDay() {
    this._loadingService.show();
    this._spendingService.getSpendingToDay().subscribe(
      (data: ISpendingModel[]) => {
        this.dataSource.data = data;
   
        this._loadingService.hide();
      },(error) => {
        this._loadingService.hide();
        this._messageService.showMessage(error);
      }
    );
  }
  //#endregion
}