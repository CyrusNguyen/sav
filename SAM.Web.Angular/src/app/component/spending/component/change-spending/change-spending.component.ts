import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { ISpendingModel } from 'src/app/models/ISpendingModel';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoadingService } from 'src/app/services/loading.service';
import { MessageService } from 'src/app/services/message.service';
import { ValidationService } from 'src/app/services/commons/valid.service';

export interface Wallet {
  WalletCode: string;
  Name: string;
}

@Component({
  selector: 'app-change-spending',
  templateUrl: './change-spending.component.html',
  styleUrls: ['./change-spending.component.scss']
})
export class ChangeSpendingComponent implements OnInit {

  //#region Variables
  private form: FormGroup;
  private wallets: Wallet[] = [
    { WalletCode: 'A1', Name: 'Default' },
    { WalletCode: 'A2', Name: 'Momo' },
    { WalletCode: 'A3', Name: 'TPBank' }
  ];
  @Input() element: ISpendingModel = {
    Id: null,
    Code: '',
    Title: '',
    Description: '',
    Expense: null,
    PaymentDate: new Date(),
    UserCode: '',
    WalletCode: this.wallets.length > 0 ? this.wallets[0].WalletCode : null
  };
  //#endregion

  //#region Constructors
  constructor(
    private _formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ChangeSpendingComponent>,
    private _loadingService: LoadingService,
    private _messageService: MessageService,
    private _validationService: ValidationService,
    @Inject(MAT_DIALOG_DATA) public data: ISpendingModel) { }
  //#endregion

  //#region Form Control
  get PaymentDate() { return this.form.get('PaymentDate'); }
  get Title() { return this.form.get('Title'); }
  get Description() { return this.form.get('Description'); }
  get Expense() { return this.form.get('Expense'); }
  get Wallet() { return this.form.get('Wallet'); }

  get Header() {
    let header: string = "Create new spending";

    if (this.data !== null) {
      this.element = this.data;
      header = `Edit spending - ${this.data.Code}`;
    }

    return header;
  }

  ngOnInit() {
    if (this.data !== null) {
      this.element = this.data;
    }

    this.form = this._formBuilder.group({
      PaymentDate: [{ value: this.element.PaymentDate, disabled: false }, Validators.compose([Validators.required, Validators.maxLength(50)])],
      Title: [this.element.Title, Validators.compose([Validators.required, Validators.maxLength(100), this._validationService.RequiredValidatior(), this._validationService.CharacterValidator()])],
      Expense: [this.element.Expense, Validators.compose([Validators.pattern('^[0-9]*$'), this._validationService.ScopeValidator(500, 700000)])],
      Description: [this.element.Description, Validators.compose([Validators.maxLength(500), this._validationService.CharacterValidator()])],
      Wallet: [this.element.WalletCode, Validators.compose([Validators.required, Validators.maxLength(70)])]
    });
  }
  //#endregion

  //#region Methods 
  save(){
    //this._loadingService.show();  
  }
  cancel() {
    this.dialogRef.close();
  }
  //#endregion
}
