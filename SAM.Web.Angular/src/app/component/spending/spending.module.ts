import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatButtonModule      ,
  MatCheckboxModule    ,
  MatTooltipModule     ,
  MatSidenavModule     ,
  MatToolbarModule     ,
  MatIconModule        ,
  MatCardModule        ,
  MatDividerModule     ,
  MatListModule        ,
  MatExpansionModule   ,
  MatFormFieldModule   ,
  MatTabsModule        ,
  MatMenuModule        ,
  MatInputModule       ,
  MatDatepickerModule  ,
  MatNativeDateModule  ,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatDialogModule
} from '@angular/material';

import { SpendingComponent } from './component/spending.component';
import { ChangeSpendingComponent } from './component/change-spending/change-spending.component';
import { SpendingRoutingModule } from './spending-routing.module';
import { LoadingComponent } from '../shares/loading/loading.component';

@NgModule({
  declarations: [
    SpendingComponent, 
    ChangeSpendingComponent
  ],
  imports: [
    CommonModule,
    SpendingRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    // define modules of material angular.
    MatButtonModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatTabsModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatDialogModule
  ],
  providers: [],
  entryComponents: [
    ChangeSpendingComponent
  ]
})
export class SpendingModule { }
