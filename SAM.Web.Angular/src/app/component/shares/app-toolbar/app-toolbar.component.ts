import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserComponent } from '../../user/component/user.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-toolbar',
  templateUrl: './app-toolbar.component.html',
  styleUrls: ['./app-toolbar.component.scss']
})
export class AppToolbarComponent implements OnInit {

  //#region System
  constructor(private router: Router,
    public dialog: MatDialog) { }

  get Title() {
    let url = this.router.url.replace('/', '');
    switch (url) {
      case 'spending':
        return 'Spending';
      case 'wallet':
        return 'Wallet';
      case 'profile':
        return 'Profile';
      default: return 'Spending';
    }
  }

  ngOnInit() {
  }
  //#endregion

  //#region Method
  dialogProfile(): void {
    const dialogRef = this.dialog.open(UserComponent, {});

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  //#endregion
}
