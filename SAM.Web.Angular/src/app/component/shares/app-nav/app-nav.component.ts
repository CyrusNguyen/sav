import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-nav',
  templateUrl: './app-nav.component.html',
  styleUrls: ['./app-nav.component.scss']
})
export class AppNavComponent implements OnInit {

  //#region System
  constructor(
    private titleService: Title) {

  }

  ngOnInit() {

  }
  //#endregion

  //#region Method
  setTitle(title: string) {
    this.titleService.setTitle(title);
  }
  //#endregion
}
