import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ValidationService } from 'src/app/services/commons/valid.service';
import { MessageService } from 'src/app/services/message.service';
import { LoadingService } from 'src/app/services/loading.service';
import { delay } from 'q';
import { UserService } from 'src/app/services/user.service';
import { UserSignInViewModel, UserSignUpViewModel, Role } from 'src/app/models/IUserModel';
//
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  //#region Systems
  private form: FormGroup;
  private hide = true;

  constructor(
    private formBuilder: FormBuilder,
    private validationService: ValidationService,
    private messageService: MessageService,
    private loadingService: LoadingService,
    private userService: UserService) {
  }

  // define abstraction control
  get Username() { return this.form.get('Username'); }
  get Fullname() { return this.form.get('Fullname'); }
  get Phone() { return this.form.get('Phone'); }
  get Password() { return this.form.get('Password'); }
  get ConfirmPassword() { return this.form.get('ConfirmPassword'); }

  ngOnInit() {
    this.form = this.formBuilder.group({
      Username: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(6),
        this.validationService.NoWhiteSpaceValidator(),
        this.validationService.CharacterValidator()])],
      Fullname: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100),
        this.validationService.CharacterValidator()])],
      Phone: ['', Validators.compose([
        Validators.pattern('^[0-9]*$'),
        Validators.maxLength(10),
        Validators.minLength(10)])],
      Password: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(8)])],
      ConfirmPassword: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(8)])]
    });
  }
  //#endregion

  register() {

    if ( this.Password.value !== this.ConfirmPassword.value ) {
      this.messageService.showMessage('Password not match', true);
      return;
    }

    // this.messageService.showMessage('Lỗi', true);
    this.loadingService.show();

    const useSignup = new UserSignUpViewModel();
    useSignup.Username  = this.Username.value;
    useSignup.Password  = this.Password.value;
    useSignup.FullName  = this.Fullname.value;
    useSignup.Phone     = this.Phone.value;
    useSignup.Role      = 'User';

    this.userService.register(useSignup).subscribe(data => {
      this.loadingService.hide();
    }, error => {
      this.loadingService.hide();
      this.messageService.showMessage(error.error.Message, true);
    });

  }
}
