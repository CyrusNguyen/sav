import { NgModule } from '@angular/core';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './component/register.component';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule      ,
  MatCheckboxModule    ,
  MatTooltipModule     ,
  MatSidenavModule     ,
  MatToolbarModule     ,
  MatIconModule        ,
  MatCardModule        ,
  MatDividerModule     ,
  MatListModule        ,
  MatExpansionModule   ,
  MatFormFieldModule   ,
  MatTabsModule        ,
  MatMenuModule        ,
  MatInputModule       ,
  MatDatepickerModule  ,
  MatNativeDateModule  ,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//
@NgModule({
  declarations: [
    RegisterComponent
  ],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    //
    FormsModule,
    ReactiveFormsModule,
    
    // define modules of material angular.
    MatButtonModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatTabsModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule
  ]
})
export class RegisterModule { }