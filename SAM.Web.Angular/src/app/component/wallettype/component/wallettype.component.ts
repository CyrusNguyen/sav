import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IColumn } from 'src/app/models/IColumn';
import { IWalletTypeModel } from 'src/app/models/IWalletTypeModel';
import { MatSort, MatDialog } from '@angular/material';
import { ChangeWalletTypeComponent } from './change-wallettype/change-wallettype.component';
import { WalletTypeService } from 'src/app/services/walettype.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MessageService } from 'src/app/services/message.service';
import { UserResultViewModel } from 'src/app/models/IUserModel';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-wallet-type',
  templateUrl: './wallettype.component.html',
  styleUrls: ['./wallettype.component.scss']
})
export class WalletTypeComponent implements OnInit {

  //#region Variables
  private dataSource = new MatTableDataSource<IWalletTypeModel>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  //
  private columnsToDisplay: IColumn[] = [
    { Code: 'Key', Format: 'Key' },
    { Code: 'Name', Format: 'Name' }
  ];
  private displayedColumns = this.columnsToDisplay.map((column: IColumn) => column.Code);
  private _user : UserResultViewModel;
  //#endregion

  //#region Constructors
  constructor(
    private _walletTypeService: WalletTypeService,
    private _dialog: MatDialog,
    private _loadingService: LoadingService,
    private _messageService: MessageService,
    private _authenticationService:AuthenticationService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this._user = this._authenticationService.getUser();

    this.showWalletType();
  }
  //#endregion

  //#region Methods
  formatData(column: string, value: IWalletTypeModel) {
    return value[column];
  }

  fillterForWalletType(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showDialog(element: IWalletTypeModel = null): void {
    const dialogRef = this._dialog.open(ChangeWalletTypeComponent, {
      data: element
    });

    dialogRef.afterClosed().subscribe(() => {
      this.showWalletType();
    });
  }

  showWalletType() {
    this._walletTypeService.getWalletTypeByUser(this._user.Username).subscribe(
      (data: IWalletTypeModel[]) => {
        this.dataSource.data = data;
      },(error) => {
        this._messageService.showMessage(error.message);
      }
    );
  }
  //#endregion
}