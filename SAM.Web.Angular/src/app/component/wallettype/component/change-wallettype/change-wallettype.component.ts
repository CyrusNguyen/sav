import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { IWalletTypeModel } from 'src/app/models/IWalletTypeModel';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ValidationService } from 'src/app/services/commons/valid.service';

@Component({
  selector: 'app-change-wallettype',
  templateUrl: './change-wallettype.component.html',
  styleUrls: ['./change-wallettype.component.scss']
})
export class ChangeWalletTypeComponent implements OnInit {
  //#region Variables
  private form: FormGroup;
  @Input() element: IWalletTypeModel = {
    Id: null,
    Key: null,
    Name: null,
    IsDefault: false,
    UserCode: null
  };
  //#endregion

  //#region Contructors
  constructor(
    private _formBuilder: FormBuilder,
    private _dialogRef: MatDialogRef<ChangeWalletTypeComponent>,
    private _validationService : ValidationService,
    @Inject(MAT_DIALOG_DATA) public data: IWalletTypeModel) { }
    
  //#endregion

  //#region Form Control
  get Key() { return this.form.get('Key'); }
  get Name() { return this.form.get('Name'); }
  get Header() {
    let header: string = "Create new wallet type";

    if (this.data !== null) {
      this.element = this.data;
      header = `Edit wallet type - ${this.data.Key}`;
    }

    return header;
  }

  ngOnInit() {
    if (this.data !== null) {
      this.element = this.data;
    }

    this.ngForm();
  }

  ngForm(){
    this.form = this._formBuilder.group({
      Key: [this.element.Key, Validators.compose([Validators.required, Validators.maxLength(20), this._validationService.NoWhiteSpaceValidator(),this._validationService.CharacterValidator()])],
      Name: [this.element.Name, Validators.compose([Validators.required, Validators.maxLength(500),this._validationService.CharacterValidator()])],
    });
  }
  //#endregion

  //#region Methods 
  cancel() {
    this._dialogRef.close();
  }
  //#endregion
}