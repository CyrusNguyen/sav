import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//
import { WalletComponent } from './component/wallet.component';
import { ChangeWalletComponent } from './component/change-wallet/change-wallet.component';
import { WalletRoutingModule } from './wallet-routing.module';
//
import { WalletTypeComponent } from '../wallettype/component/wallettype.component';
import { ChangeWalletTypeComponent } from '../wallettype/component/change-wallettype/change-wallettype.component';
//
import {
  MatButtonModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatDividerModule,
  MatListModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatTabsModule,
  MatMenuModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatDialogModule
} from '@angular/material';
//
@NgModule({
  declarations: [
    // Component for Wallet
    WalletComponent,
    ChangeWalletComponent,

    // Component for Wallet Type
    WalletTypeComponent,
    ChangeWalletTypeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    // Define routing for wallet 
    WalletRoutingModule,

    // define modules of material angular.
    MatButtonModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatTabsModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatDialogModule
  ],
  providers: [],
  entryComponents: [
    ChangeWalletComponent,
    ChangeWalletTypeComponent
  ]
})
export class WalletModule { }
