import { RouterModule, Routes } from '@angular/router';
import { WalletComponent } from './component/wallet.component';

const routes: Routes = [
  {
    path: '',
    component: WalletComponent
  },
];

export const WalletRoutingModule = RouterModule.forChild(routes);
