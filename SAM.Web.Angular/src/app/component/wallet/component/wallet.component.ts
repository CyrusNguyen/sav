import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IColumn } from 'src/app/models/IColumn';
import { IWalletModel } from 'src/app/models/IWalletModel';
import { MatSort, MatDialog } from '@angular/material';
import { ChangeWalletComponent } from './change-wallet/change-wallet.component';
import { HelperService } from 'src/app/services/commons/helper.service';
import { UserResultViewModel } from 'src/app/models/IUserModel';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MessageService } from 'src/app/services/message.service';
import { WalletService } from 'src/app/services/wallet.service';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {

  //#region Variables
  private dataSource = new MatTableDataSource<IWalletModel>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  //
  private columnsToDisplay: IColumn[] = [
    { Code: 'Code', Format: 'Code' },
    { Code: 'Name', Format: 'Name' },
    { Code: 'Balance', Format: 'Balance' },
    { Code: 'TypeName', Format: 'TypeName' }
  ];
  private displayedColumns = this.columnsToDisplay.map((column: IColumn) => column.Code);
  //#endregion

  //#region Constructors
  constructor(
    private _walletService: WalletService,
    private _dialog: MatDialog,
    private _helpService: HelperService,
    private _loadingService: LoadingService,
    private _messageService: MessageService,
    private _authenticationService:AuthenticationService) {
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.showWallet();
  }
  //#endregion

  //#region Methods
  formatData(column: string, value: IWalletModel) {
    if (column === 'Balance') {
      return this._helpService.formatCurrency(value[column]);
    }

    return value[column];
  }

  fillterForWallet(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showDialog(element: IWalletModel = null): void {
    const dialogRef = this._dialog.open(ChangeWalletComponent, {
      data: element
    });

    dialogRef.afterClosed().subscribe(() => {
      this.showWallet();
    });
  }

  showWallet() {
    this._loadingService.show();

    this._walletService.getWalletByUser(this._authenticationService.getUser().Username).pipe(delay(100)).subscribe(
      (data: IWalletModel[]) => {
        this.dataSource.data = data;

        this._loadingService.hide();
      },(error) => {
        this._messageService.showMessage(error.message);

        this._loadingService.hide();
      }
    );
  }
  //#endregion
}
