import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { IWalletModel } from 'src/app/models/IWalletModel';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ValidationService } from 'src/app/services/commons/valid.service';
import { WalletTypeService } from 'src/app/services/walettype.service';
import { WalletService } from 'src/app/services/wallet.service';
import { IWalletTypeModel } from 'src/app/models/IWalletTypeModel';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-change-wallet',
  templateUrl: './change-wallet.component.html',
  styleUrls: ['./change-wallet.component.scss']
})
export class ChangeWalletComponent implements OnInit {

  //#region Variables
  private form: FormGroup;
  private wallettypes: IWalletTypeModel[] = [];
  @Input() element: IWalletModel = {
    Code: null,
    Name: '',
    Balance: null,
    TypeId: this.wallettypes.length > 0 ? this.wallettypes[0].Id : null,
    TypeKey: this.wallettypes.length > 0 ? this.wallettypes[0].Key : null,
    TypeName: this.wallettypes.length > 0 ? this.wallettypes[0].Name : null,
    UserCode: ''
  };
  //#endregion

  //#region Contructors
  constructor(
    private _formBuilder: FormBuilder,
    private _dialogRef: MatDialogRef<ChangeWalletComponent>,
    private _validationService: ValidationService,
    private _messageService:MessageService,
    private _walletService : WalletService,
    private _walletTypeService: WalletTypeService,
    private _authenticationService :AuthenticationService,
    @Inject(MAT_DIALOG_DATA) public data: IWalletModel) { }
  //#endregion

  //#region Form Control
  get Code() { return this.form.get('Code'); }
  get Name() { return this.form.get('Name'); }
  get Balance() { return this.form.get('Balance'); }
  get WalletType() { return this.form.get('WalletType'); }
  get Header() {
    let header: string = "Create new wallet";

    if (this.data !== null) {
      this.element = this.data;
      header = `Edit wallet - ${this.data.Code}`;
    }

    return header;
  }

  ngOnInit() {
    console.log(this.data);
    if (this.data !== null) {
      this.element = this.data;
    }

    console.log(this.element);

    this.getWalletType();
    this.ngForm();
  }

  ngForm(){
    this.form = this._formBuilder.group({
      Code: [this.element.Code, Validators.compose([Validators.required, Validators.maxLength(100), this._validationService.NoWhiteSpaceValidator(), this._validationService.CharacterValidator()])],
      Name: [this.element.Name, Validators.compose([Validators.required, Validators.maxLength(100),  this._validationService.CharacterValidator()])],
      Balance: [this.element.Balance, Validators.compose([Validators.pattern('^[0-9]*$'), this._validationService.ScopeValidator(0, 10000000)])],
      WalletType: [this.element.TypeId, Validators.compose([Validators.required, Validators.maxLength(70)])]
    });
  }
  //#endregion

  //#region Methods 
  getWalletType() {
    this._walletTypeService.getWalletTypeByUser(this._authenticationService.getUser().Username).subscribe(
      (data: IWalletTypeModel[]) => {
        this.wallettypes = data;
        console.log(data);
        console.log(this.wallettypes);
        
      },(error) => {
        this._messageService.showMessage(error.message);
      }
    );
  }

  cancel() {
    this._dialogRef.close();
  }
  //#endregion
}