import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ValidationService } from 'src/app/services/commons/valid.service';
//
export interface User {
  Username: string;
  Fullname: string;
  Phone: string;
}
//
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  //#region System
  private form: FormGroup;

  private user: User = {
    Username: 'VuNT22',
    Fullname: 'Nguyen Truong Vu',
    Phone: '0353422845'
  }

  constructor(
    private _formBuilder: FormBuilder,
    private _dialogRef: MatDialogRef<UserComponent>,
    private _validationService: ValidationService) {
  }

  // define abstraction control
  get Username() { return this.form.get('Username'); }
  get Fullname() { return this.form.get('Fullname'); }
  get Phone() { return this.form.get('Phone'); }

  ngOnInit() {
    this.form = this._formBuilder.group({
      Username: [this.user.Username, Validators.compose([Validators.required, Validators.maxLength(500), this._validationService.NoWhiteSpaceValidator()])],
      Fullname: [this.user.Fullname, Validators.compose([Validators.required, Validators.maxLength(100)])],
      Phone: [this.user.Phone, Validators.compose([Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(10)])]
    });
  }
  //#endregion

  //#region Methods 
  cancel() {
    this._dialogRef.close();
  }
  //#endregion
}
