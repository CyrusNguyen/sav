import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './component/user.component';

import {
  MatButtonModule      ,
  MatCheckboxModule    ,
  MatTooltipModule     ,
  MatSidenavModule     ,
  MatToolbarModule     ,
  MatIconModule        ,
  MatCardModule        ,
  MatDividerModule     ,
  MatListModule        ,
  MatExpansionModule   ,
  MatFormFieldModule   ,
  MatTabsModule        ,
  MatMenuModule        ,
  MatInputModule       ,
  MatDatepickerModule  ,
  MatNativeDateModule  ,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule
} from '@angular/material';
//
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

//
@NgModule({
  declarations: [
    UserComponent
  ],
  imports: [
    CommonModule,
    //
    FormsModule,
    ReactiveFormsModule,
    
    // define modules of material angular.
    MatButtonModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatTabsModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule
  ],
  entryComponents: [UserComponent]
})
export class UserModule { }
