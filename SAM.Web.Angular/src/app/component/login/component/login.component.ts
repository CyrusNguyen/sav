import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, ValidationErrors } from '@angular/forms';
import { ValidationService } from 'src/app/services/commons/valid.service';
//
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  //#region System
    private form: FormGroup;
    private hide:boolean = true;

    //#region Constructor
    constructor(
      private _formBuilder: FormBuilder,
      private _validationService: ValidationService) {
    }
    //#endregion

    // define abstraction control
    get Username() { return this.form.get('Username'); }
    get Password() { return this.form.get('Password'); }

    //#region Initialize
    ngOnInit() {
      this.form = this._formBuilder.group({
        Username: ['', Validators.compose([
          Validators.required,
          // tslint:disable-next-line: max-line-length
          Validators.maxLength(100),
          Validators.minLength(6),
          this._validationService.NoWhiteSpaceValidator(),
          this._validationService.CharacterValidator()])],
        Password: ['', Validators.compose([Validators.required, Validators.maxLength(100),Validators.minLength(6)])],
      });
    }

    login() {
      console.log(this.Username.errors);
    }
    //#endregion
  //#endregion
}
