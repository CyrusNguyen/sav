import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './component/login.component';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule      ,
  MatCheckboxModule    ,
  MatTooltipModule     ,
  MatSidenavModule     ,
  MatToolbarModule     ,
  MatIconModule        ,
  MatCardModule        ,
  MatDividerModule     ,
  MatListModule        ,
  MatExpansionModule   ,
  MatFormFieldModule   ,
  MatTabsModule        ,
  MatMenuModule        ,
  MatInputModule       ,
  MatDatepickerModule  ,
  MatNativeDateModule  ,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//
@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    //
    FormsModule,
    ReactiveFormsModule,
    
    // define modules of material angular.
    MatButtonModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatTabsModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule
  ]
})
export class LoginModule { }