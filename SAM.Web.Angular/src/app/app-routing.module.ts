import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SpendingModule } from './component/spending/spending.module';
import { WalletModule } from './component/wallet/wallet.module';
import { RegisterModule } from './component/register/register.module';
import { LoginModule } from './component/login/login.module';
import { UserModule } from './component/user/user.module';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => LoginModule
  },
  {
    path: 'register',
    loadChildren: () => RegisterModule
  },
  {
    path: 'spending',
    loadChildren: () => SpendingModule
  },
  {
    path: 'wallet',
    loadChildren: () => WalletModule
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: '*',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false
    })
  ],
  exports: [
    RouterModule,
    LoginModule,
    RegisterModule,
    SpendingModule,
    WalletModule ,
    UserModule
  ],
})
export class AppRoutingModule { }
