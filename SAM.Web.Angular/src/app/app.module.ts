import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//
import { AppRoutingModule } from './app-routing.module';
//
import { AppComponent } from './component/app.component';
import { CommonModule } from '@angular/common';
//
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppNavComponent } from './component/shares/app-nav/app-nav.component';
import { AppFooterComponent } from './component/shares/app-footer/app-footer.component';
//
import {
  MatButtonModule      ,
  MatCheckboxModule    ,
  MatTooltipModule     ,
  MatSidenavModule     ,
  MatToolbarModule     ,
  MatIconModule        ,
  MatCardModule        ,
  MatDividerModule     ,
  MatListModule        ,
  MatExpansionModule   ,
  MatFormFieldModule   ,
  MatTabsModule        ,
  MatMenuModule        ,
  MatInputModule       ,
  MatDatepickerModule  ,
  MatNativeDateModule  ,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatSnackBarModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppToolbarComponent } from './component/shares/app-toolbar/app-toolbar.component';
import { MessageComponent } from './component/shares/message/message.component';
import { LoadingComponent } from './component/shares/loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    AppNavComponent,
    AppFooterComponent,
    AppToolbarComponent,

    // component for share.
    MessageComponent,
    LoadingComponent    
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    
    FormsModule,
    ReactiveFormsModule,
    //
    AppRoutingModule,
    // define modules of material angular.
    MatButtonModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatTabsModule,
    MatMenuModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  ],
  providers: [],
  entryComponents: [
    LoadingComponent,
    MessageComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
