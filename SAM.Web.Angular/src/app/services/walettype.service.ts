import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HttpConfigService } from './httpconfig.service';
import { IWalletTypeModel } from '../models/IWalletTypeModel';
import { Observable, of, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WalletTypeService {

  private _urlBase = `${this._httpConfig.getServer()}/wallettype`;  // URL to web api

  constructor(
    private _http: HttpClient,
    private _httpConfig: HttpConfigService) {  }

  getWalletTypeByUser(username: string) : Observable<IWalletTypeModel[]>{
    const url = `${this._urlBase}/getwallettype`;
    const result = this._http.get<IWalletTypeModel[]>(url,
      {
        params:{ username: username}
      })
      .pipe(
        retry(1),
        catchError((error) => throwError(error))
      );

    return result;
  }

  delete(username: string) : Observable<IWalletTypeModel[]>{
    const url = `${this._urlBase}/getwallettype`;
    const result = this._http.get<IWalletTypeModel[]>(url,
      {
        params:{ username: username}
      })
      .pipe(
        retry(1),
        catchError((error) => throwError(error))
      );

    return result;
  }

  add(username: string): Observable<IWalletTypeModel[]>{
    const url = `${this._urlBase}/getwallettype`;
    const result = this._http.get<IWalletTypeModel[]>(url,
      {
        params:{ username: username}
      })
      .pipe(
        retry(1),
        catchError((error) => throwError(error))
      );

    return result;
  }

  update(username: string) : Observable<IWalletTypeModel[]>{
    const url = `${this._urlBase}/getwallettype`;
    const result = this._http.get<IWalletTypeModel[]>(url,
      {
        params:{ username: username}
      })
      .pipe(
        retry(1),
        catchError((error) => throwError(error))
      );

    return result;
  }

}