import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HttpConfigService } from './httpconfig.service';
import { throwError, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { IWalletModel } from '../models/IWalletModel';

@Injectable({
  providedIn: 'root'
})
export class WalletService {

  private _urlBase = `${this._httpConfig.getServer()}/wallet`;  // URL to web api

  constructor(
    private _http: HttpClient,
    private _httpConfig: HttpConfigService) {  }

  getWalletByUser(username: string) : Observable<IWalletModel[]>{
      const url = `${this._urlBase}/getwallet`;
      const result = this._http.get<IWalletModel[]>(url,
        {
          params:{ username: username}
        })
        .pipe(
          retry(1),
          catchError((error) => throwError(error))
        );
  
      return result;
    }
}
