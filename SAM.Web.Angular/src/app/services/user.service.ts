import { Injectable } from '@angular/core';
import { HttpConfigService } from './httpconfig.service';
import { UserSignUpViewModel } from '../models/IUserModel';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private urlBase = `${this.httpConfigService.getServer()}/user`;  // URL to web api

  constructor(
    private httpClient: HttpClient,
    private httpConfigService: HttpConfigService
  ) { }

  public register(userSignUp: UserSignUpViewModel): Observable<any> {
      const url = `${this.urlBase}/signup`;

      return this.httpClient.post(url, userSignUp);
    }

}
