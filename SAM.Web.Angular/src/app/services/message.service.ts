import { Injectable, ViewChild, ViewContainerRef } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MessageComponent } from '../component/shares/message/message.component';
import { MessageData } from '../models/IColumn';


@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private snackBar: MatSnackBar) {}

  durationInSeconds = 5;

  private show(data: MessageData) {
    // this.snackBar.open('hihihi','x',{
    //     data: data,
    //     duration: this.durationInSeconds * 1000,
    //     horizontalPosition:'right',
    //     verticalPosition: 'top'
    //   });

    this.snackBar.openFromComponent(MessageComponent, {
      data: `${data}`,
      duration: this.durationInSeconds * 1000,
      horizontalPosition: 'right',
      verticalPosition: 'top'
    });
  }

  public showMessage(message: string, error = false) {
    const data: MessageData = {
      valueMessage: message,
      isError: error
    };

    this.show(data);
  }
}
