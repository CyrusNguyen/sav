import { Injectable } from '@angular/core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { LoadingComponent } from '../component/shares/loading/loading.component';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private overlayRef: OverlayRef = null;
  constructor(private overlay: Overlay) {}

  show() {

    if (!this.overlayRef) {
      console.log(this.overlayRef);
      this.overlayRef = this.overlay.create();
    }
    // Create ComponentPortal that can be attached to a PortalHost
    const spinnerOverlayPortal = new ComponentPortal(LoadingComponent);

    this.overlayRef.attach(spinnerOverlayPortal);
  }

  hide() {
    if (!!this.overlayRef) {
      this.overlayRef.detach();
    }
  }
}
