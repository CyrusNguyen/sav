import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserResultViewModel } from '../models/IUserModel';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private router: Router) { }

  public loginUrl(){
    this.router.navigate(['/login']);
  }

  public logoutUrl(){
    this.router.navigateByUrl('/logout');
  }

  public logoutAndRemoveUserToken(){
    this.removeUserToken();
    this.router.navigateByUrl('/logout');
  }

  public getUserToken() :string {
    return localStorage.getItem('userToken');
  }

  public getUser(): UserResultViewModel{
    let user = new UserResultViewModel();
    user.Username = "VuNT22";
    return user;
  }

  public removeUserToken(): void{
    localStorage.removeItem('userToken');
  }
}
