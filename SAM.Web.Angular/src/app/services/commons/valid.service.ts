import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl } from '@angular/forms';
import { HelperService } from './helper.service';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor(private _helperService: HelperService) { }

  public NoWhiteSpaceValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const isValid = (control.value || '').indexOf(" ") !== -1;
      return !isValid ? null : { 'whitespace': 'The value is not contain whitespace' }
    };
  }

  public RequiredValidatior(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const isValid = (control.value || '').trim() !== '';
      return isValid ? null : { 'requiredValid': '' }
    };
  }

  public CharacterValidator(characters = ['"', '+', '-', '.', ';', '\'']): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

      let stringContain = "";
      characters.forEach(element => {
        const index = (control.value || '').indexOf(element);
        if (index !== -1) {
          stringContain += `${element} `;
        }
      });

      return stringContain.length === 0 ? null : { 'characterValid': stringContain }
    };
  }

  public ScopeValidator(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const value = parseFloat((control.value || ''));
      const isScope = value > max || value < min;

      return !isScope ? null : { 'scope': 'The value must be between ' + 
      this._helperService.formatCurrency(min) + 'đ and ' + this._helperService.formatCurrency(max) + 'đ.' }
    };
  }
}