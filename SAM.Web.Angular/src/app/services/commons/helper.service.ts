import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ValidatorFn, AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }

  public formatDate(value: Date){
    var datePipe = new DatePipe('en-US');
    return datePipe.transform(value, 'MM/dd/yyyy');
  }

  public formatStringLimitLenght(value: string,length: number = 50){
    if(value.length >= length)
    {
      return value.substring(0,length) + '...';
    }
    return value;
  }

  public jsonToObject(json:any): any{
    return JSON.parse(json);
  }

  public objectToJSON(object:any): string{
    return JSON.stringify(object);
  }

  public formatText(alias: string):string {
    let str = alias;
    str = str.toString().replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.toString().replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.toString().replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.toString().replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.toString().replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.toString().replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.toString().replace(/đ/g,"d");
    str = str.toString().replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,"-");
    str = str.toString().replace(/ + /g,"-");
    str = str.toString().trim(); 
    
    return str;
  }

  public formatCurrency(value: number){
    return Intl.NumberFormat('en-CAD', { currency: 'CAD', style: 'decimal'}).format(value);
  }

  public preventInput(event: any,value: number){
    if (value >= 100){
      return event.preventDefault();
    }
  }
}