import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpConfigService {
  private _url = 'http://localhost:21668/api';  // URL to web api

  private _options = {
    headers: new HttpHeaders({ 'Accept': '*/*', 'Content-Type': 'application/json' })
  };

  constructor() { }

  public getServer(): string {
    return this._url;
  }

  public getOption() {
    return this._options;
  }
}
