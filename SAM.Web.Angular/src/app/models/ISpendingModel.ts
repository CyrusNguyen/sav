export interface ISpendingModel {
    Id?: number;
    Code?: string;
    Title?: string;
    Description?: string;
    Expense?: number;
    PaymentDate?: Date;
    UserCode?: string;
    WalletCode?: string;
}

export class SpendingResultViewModel {
    Id?: number;
    Code?: string;
    Title?: string;
    Description?: string;
    Expense?: number;
    PaymentDate?: Date;
    UserCode?: string;
}

export class SpendingAddViewModel {
    Title?: string;
    Description?: string;
    Expense?: number;
    PaymentDate?: Date;
    UserCode?: string;
    WalletCode?: string;
}

export class SpendingDeleteViewModel {
    Code?: string;
    UserCode?: string;
}

export class SpendingUpdateViewModel extends SpendingAddViewModel{
    Code?: string;
}