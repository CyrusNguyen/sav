export interface IUserModel {

}

export class UserPasswordViewModel {
    Code?: string;
    Username?: string;
    PasswordOld?: string;
    PasswordNew?: string;
    TokenCode?: string;
}

export class UserResultViewModel {
    Code?: string;
    Username?: string;
    TokenCode?: string;
    FullName?: string;
    Phone?: string;
    Role?: string;
}

export class UserSignInViewModel {
    Username?: string;
    Password?: string;
}

export class UserSignUpViewModel extends UserSignInViewModel {
    FullName?: string;
    Phone?: string;
    Role?: string;
}

export class UserUpdateViewModel {
    Code?: string;
    Username?: string;
    Password?: string;
    TokenCode?: string;
    FullName?: string;
    Phone?: string;
}

export enum Role {
    ADMIN,
    USER
}
