export interface IColumn {
    Code: string;
    Format: string;
}

export interface MessageData {
    valueMessage?: string;
    isError: boolean;
}