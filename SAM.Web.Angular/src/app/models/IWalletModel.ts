export interface IWalletModel{
    Code : string;
    Name :string;
    Balance :number;
    TypeId :number;
    TypeKey :string;
    TypeName :string;
    UserCode :string;    
}

export class WalletResultViewModel{
    Code? :string;
    Name? :string;
    Balance ?:number;
    TypeCode ?:number;
    UserCode ?:string;   
}

// wallet
export class WalletAddViewModel{
    Name ?:string;
    Balance ?:number;
    TypeCode ?:number;
    UserCode ?:string;
}

export class WalletDeleteViewModel{
    WalletCode? :string;
    UserCode ?:string;
}

export class WalletUpdateViewModel{
    Name ?:string;
    UserCode ?:string;
    TypeCode ?:number;
    WalletCode? :string;
}

// money of wallet
export class WalletMoneyViewModel{
    WalletCode? :string;
    Amount ?:number;
    Description ?:number;
    UserCode ?:string;   
}

export class WalletMoveMoneyViewModel{
    WalletCode? :string;
    ReferenceCode? :string;
    Description ?:number;
    Expense ?:number;
    UserCode ?:string;   
}