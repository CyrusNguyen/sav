export interface IWalletTypeModel {
    Id?: number;
    Key?: string;
    Name?: string;
    IsDefault?: boolean;
    UserCode?: string;
}

export class WalletTypeResultViewModel {
    Id?: number;
    Key?: string;
    Name?: string;
    IsDeleted?: boolean;
    IsDefault?: boolean;
    UserCode?: string;
}

export class WalletTypeAddViewModel {
    Key?: string;
    Name?: string;
    UserCode?: string;
}

export class WalletTypeDeleteViewModel {
    Id?: number;
    UserCode?: string;
}

export class WalletTypeUpdateViewModel {
    Id?: number;
    Key?: string;
    Name?: string;
    UserCode?: string;
}

export class WalletTypeDefaultViewModel extends WalletTypeDeleteViewModel { }