import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { tap, catchError } from "rxjs/operators";
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
 
    constructor(
        private router: Router,
        private _authenService: AuthenticationService
    ) { }
 
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
              
        if (req.headers.get('No-Auth') == "True")
            return next.handle(req.clone());
 
        if ( this._authenService.getUserToken() != null) {
            const clonedreq = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + this._authenService.getUserToken())
            });

            return next.handle(clonedreq).pipe(tap(
                (success) => { },
                (error) => {
                    if (error.status === 401)
                    {
                        this._authenService.logoutAndRemoveUserToken();
                    }
                }
                ));
        }
        else  {
            this._authenService.logoutAndRemoveUserToken();
        }
    }
}